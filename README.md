# ViBrow

## Description

ViBrow (*Vi*deo *Brow*ser) is a rust TUI application that allows you to browse and consume video 
content from various platforms. It features a keybinds system inspired by 
[Vim](https://github.com/vim/vim) and an interface inspired by [Vifm](https://github.com/vifm/vifm). 
It is designed to be highly configurable and minimalistic and is particularly well-suited for Linux 
enthusiasts.

![demo](./assets/demo.png)

## Build Process

Make sure you have the latest version of Rust installed on your system and then run

``` sh
cargo build --release
```

## Configuration

Currently, the location of the configuration file is set and cannot be changed. On Linux systems, 
the file is located in `$XDG_CONFIG_HOME`/vibrow/config.toml, on Mac systems it can be found at 
`$HOME`/Library/Application Support/vibrow/config.toml and on Windows systems, it can be found in 
`{FOLDERID_RoamingAppData}`\vibrow\config.toml.

We highly recommend customizing your settings to suit your needs, including selecting a preferred
video player, downloader, and text editor. It is important to ensure that your chosen video player
and downloader support the m3u8 format.

## Usage

### Layout

When the application launches, it will be divided into three primary sections: a title block, two 
content blocks and a status/command block.

#### Title Block

The title block, located at the top of the window, currently only shows the name of the application.

#### Content Blocks

The core of the application is comprised of content blocks, which can present either a list of 
navigable items or the details of a selected item. The focused block is where all operations take 
place, and switching focus can be achieved by pressing the `Tab` key.

The Quick View feature is enabled by default, allowing the unfocused block to show the details of the
selected item. If you wish to turn off this feature, simply press the `w` key, and the unfocused 
block will display the contents of another provider instead. This feature is particularly useful for 
working with two providers simultaneously. Currently, the feature lacks usefulness when working with 
the same provider in both blocks since they share the same state.

#### Status/Input Block

The status block can be found at the bottom of the window, displaying the most recent logged message 
when in normal mode. However, it functions as a textbox when in interactive mode, which can be 
accessed by pressing the `:` key.

### Navigation

- `j` or `↑` - select previous item in the list
- `k` or `↓` - select next item in the list
- `h` or `←` - move up one level
- `l` or `→` - go into the selected item
- `gg` or `Home` - select first item in the list
- `G` or `End` - select the last item in the list
- `<` and `>` - cycle providers for the focused block
- `Tab` - toggle focus
- `v` or `V` - toggle visual (multiple selection) mode

### Commands

Enter the command mode and input the desired command for execution.

- `q` or `exit` - exit the application
- `go <up|down|into|out|top|bottom|<number>>` - selects item in the list
- `/<query>` - finds item in the list that matches the query
- `!<shell command>` - runs a shell command
- `set <key>=<value>` - sets value to the variable
- `toggle <mode|focus|quickview>` - toggles the value of a binary property
- `yank` - copies the selected items to the clipboard
- `delete` - moves selected items to the clipboard
- `paste` - pastes selected items from the clipboard
- `save <destination>` - saves selected items to the destination provided as a relative path
- `rename <name>` - renames selected item
- `mkdir <name>` - creates a directory
- `refresh|reload` - reloads all items in a focused block
- `load` - loads more items

#### Variables

- `mode` - `normal|visual`
- `focus` - `left|right`
- `quickview` - `on|off`
- `provider` - `next|prev[ious]` or any implemented provider name in kebab-case

#### Placeholders

Commands have support for different placeholders, which are evaluated to actual values during
runtime.

- `{<prompt>;<default>}` - asks user for input which will be inserted into command
- `[[prefix]<object>[.attribute][suffix][delimiter]]` - replaces the placeholder with the requested
value.

Valid objects and their attributes:

- `item` - refers to a currently selected items. Requires one of the following attributes:
  - `id` - id of an item
  - `name` - name of the item
  - `kind` - kind of the item
  - `path` - relative path to the item
  - `links` - stream links of the item
- `log` - list of logs in the current session
  - has optional attribute `path` which will return the path to the log file instead of list of the
  log entries
- `cwd` - relative path to the current location in the provider's tree

##### Examples

`save {destination: ;[cwd]}` - asks user for the destination path and sets the current location as
a starting value, and then saves the item

`!echo [hello item.name. ]` - evaluates to `!echo hello Item_1_Name. hello Item_2_Name.`

## Project Structure

The Elm Architecture principles are being followed by this application. It is an architecture pattern
where the model, view, and update (which represent user actions) are the three main components. The 
model contains the application state, the view displays the state, and the update modifies the state. 
This pattern provides a simple and scalable way to build user interfaces, and it promotes a functional
programming style.

- `main.rs` - contains `Model` structure, `Msg` enum, and, `view`, `update` and `subsctiptions` 
functions
- `config.rs` - contains default config and structures that mirror ones in the config file
- `logger.rs` - defines and implements logger and how logs are displayed
- `error.rs` - defines the error type that encompasses all the errors that can happen
- `utils.rs` - module that contains various types that are not tied to the application
- `textbox.rs` - defines and implements text box widget
- `tree.rs` - defines and implements widget that allows for navigation through a list of items
and the ability to load those items and their details
- `details.rs` - module that defines details widgets for every type of item from each provider

## License

ViBrow is licensed under [GPL 3.0](COPYING).

## Support

Support the cause to watch videos without those pesky ads and creepy tracking! Let's stick it to the
man and enjoy our cat videos in peace. Rise up and support the fight for digital freedom!

- Bitcoin: bc1qlp62jes6w7zcmhmaeasvqssrt4qtvh4rd2jeww
- Monero: 88ntCRczsdzjVc74JrFgqoMfeadC66VU7ET1HNci7HNu1eqf8kViQ1HWRazVVRbbcWfQE8u1T7cpiFmLPoMrM5Q9TLo6L2o
