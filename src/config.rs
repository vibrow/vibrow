/*
 * Copyright (C) 2022 protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{collections::HashMap, path::PathBuf};

use crossterm::event::{KeyCode, MediaKeyCode, ModifierKeyCode};
use lazy_static::lazy_static;
use ratatui::style::Color;
use regex::Regex;
use serde::{Deserialize, Deserializer, Serialize};

use crate::{
    error::{Error, ErrorKind, Severity},
    utils::Trie,
};

#[rustfmt::skip]
macro_rules! CONFIG_TEMPLATE {
    () => {
r#"[keybinds]
h = 'go out'
j = 'go down'
k = 'go up'
l = 'go into'
"<left>" = 'go out'
"<down>" = 'go down'
"<up>" = 'go up'
"<right>" = 'go into'
gg = 'go top'
G = 'go bottom'
"<home>" = 'go top'
"<end>" = 'go bottom'

w = 'toggle quickview'
"<tab>" = 'toggle focus'
"<" = 'set provider=previous'
">" = 'set provider=next'
":" = '{{:;}}'
"<esc>" = 'set mode=normal'
v = 'toggle mode'
V = 'toggle mode'

"1" = 'set provider=storage'
"2" = 'set provider=gogoanime'

yd = '!yt-dlp --quiet --newline --progress --output ~/Downloads/%(title)s-%(id)s.%(ext)s [item.links ]'
yy = 'yank'
dd = 'delete'
p = 'paste'
ss = 'search {{query: ;}}'
cr = 'rename {{new name: ;[item.name]}}'
ed = '!st -e vim [item.path/desc.md ]'
el = '!st -e less [log]'
"<enter>" = '!mpv --msg-level=all=error,cplayer=info [item.links ]'
"<f5>" = 'reload'
" " = 'load'

[style]
# options: black, red, green, yellow, blue, magenta, cyan, gray, dark_gray,
#   light_red, light_green, light_yellow, light_blue, light_magenta, light_cyan, white, #RRGGBB
#foreground = "white"
#background = "black"
accent-1 = "blue"
accent-2 = "yellow"
accent-3 = "red"

[logs]
# options: trace, debug, info, warn, error, fatal
log-level = "trace"

[provider]
gogoanime = {{ url = "https://api.consumet.org/anime/gogoanime" }}
storage = {{ path = "{storage_path}" }}
"#
};}

lazy_static! {
    pub static ref DATA_DIR: PathBuf = dirs::data_dir()
        .map(|mut path| {
            path.push(env!("CARGO_PKG_NAME"));
            path
        })
        .or(dirs::home_dir().map(|mut path| {
            path.push(format!(".{}", env!("CARGO_PKG_NAME")));
            path
        }))
        .unwrap();
    pub static ref CACHE_DIR: PathBuf = dirs::cache_dir()
        .map(|mut path| {
            path.push(env!("CARGO_PKG_NAME"));
            path
        })
        .or(dirs::home_dir().map(|mut path| {
            path.push(format!(".{}", env!("CARGO_PKG_NAME")));
            path
        }))
        .unwrap();
    pub static ref DEFAULT_CONFIG: String = format!(
        CONFIG_TEMPLATE!(),
        storage_path = {
            let mut path = DATA_DIR.to_owned();
            path.push("storage");
            path
        }
        .to_str()
        .unwrap()
        .replace("\\", "\\\\"),
    );
}

/// Set of colors used by widgets
#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct ColorPalette {
    /// Primary foreground color. Inherits from terminal if not set.
    #[serde(deserialize_with = "deserialize_option_color")]
    #[serde(default)]
    pub foreground: Option<Color>,

    /// Primary background color. Inherits from terminal if not set.
    #[serde(deserialize_with = "deserialize_option_color")]
    #[serde(default)]
    pub background: Option<Color>,

    /// Primary accent color used mainly for labels
    #[serde(deserialize_with = "deserialize_color")]
    pub accent_1: Color,

    /// Secondary accent color
    #[serde(deserialize_with = "deserialize_color")]
    pub accent_2: Color,

    /// Tertiary accent color
    #[serde(deserialize_with = "deserialize_color")]
    pub accent_3: Color,
}

impl Default for ColorPalette {
    fn default() -> Self {
        Self {
            foreground: None,
            background: None,
            accent_1: Color::Blue,
            accent_2: Color::Yellow,
            accent_3: Color::Red,
        }
    }
}

/// Configuration for logger
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct LoggerConfig {
    /// Minimum severity that should be logged
    pub log_level: Severity,
}

impl Default for LoggerConfig {
    fn default() -> Self {
        Self {
            log_level: Severity::Info,
        }
    }
}

/// Config for Gogoanime
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GogoanimeConfig {
    /// Url to Gogoanime api in consumet instance
    pub url: String,
}

/// Config for local storage
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StorageConfig {
    /// Path to directory that should be used for storage
    #[serde(deserialize_with = "deserialize_path")]
    pub path: PathBuf,
}

/// Config for providers
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ProviderConfig {
    pub gogoanime: GogoanimeConfig,
    pub storage: StorageConfig,
}

/// Main config structure
#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    /// Mapping of keys and commands
    #[serde(deserialize_with = "deserialize_key_map")]
    pub keybinds: Trie<KeyCode, String>,
    pub style: ColorPalette,
    pub logs: LoggerConfig,
    pub provider: ProviderConfig,
}

/// Trait for converting to and from string for structs not defined in this project
pub trait StringExt {
    fn from_str(from: &str) -> Result<Self, Error>
    where
        Self: Sized;
    fn to_string(&self) -> String;
}

impl StringExt for KeyCode {
    fn from_str(from: &str) -> Result<Self, Error>
    where
        Self: Sized,
    {
        match from {
            "<backspace>" => Ok(KeyCode::Backspace),
            "<enter>" => Ok(KeyCode::Enter),
            "<left>" => Ok(KeyCode::Left),
            "<right>" => Ok(KeyCode::Right),
            "<up>" => Ok(KeyCode::Up),
            "<down>" => Ok(KeyCode::Down),
            "<home>" => Ok(KeyCode::Home),
            "<end>" => Ok(KeyCode::End),
            "<pageup>" => Ok(KeyCode::PageUp),
            "<pagedown>" => Ok(KeyCode::PageDown),
            "<tab>" => Ok(KeyCode::Tab),
            "<backtab>" => Ok(KeyCode::BackTab),
            "<delete>" => Ok(KeyCode::Delete),
            "<insert>" => Ok(KeyCode::Insert),
            "<null>" => Ok(KeyCode::Null),
            "<esc>" => Ok(KeyCode::Esc),
            "<capslock>" => Ok(KeyCode::CapsLock),
            "<scrolllock>" => Ok(KeyCode::ScrollLock),
            "<numlock>" => Ok(KeyCode::NumLock),
            "<prtsc>" => Ok(KeyCode::PrintScreen),
            "<break>" => Ok(KeyCode::Pause),
            "<menu>" => Ok(KeyCode::Menu),
            "<begin>" => Ok(KeyCode::KeypadBegin),
            "<play>" => Ok(KeyCode::Media(MediaKeyCode::Play)),
            "<pause>" => Ok(KeyCode::Media(MediaKeyCode::Pause)),
            "<playpause>" => Ok(KeyCode::Media(MediaKeyCode::PlayPause)),
            "<reverse>" => Ok(KeyCode::Media(MediaKeyCode::Reverse)),
            "<stop>" => Ok(KeyCode::Media(MediaKeyCode::Stop)),
            "<fastforward>" => Ok(KeyCode::Media(MediaKeyCode::FastForward)),
            "<rewind>" => Ok(KeyCode::Media(MediaKeyCode::Rewind)),
            "<next>" => Ok(KeyCode::Media(MediaKeyCode::TrackNext)),
            "<prev>" => Ok(KeyCode::Media(MediaKeyCode::TrackPrevious)),
            "<record>" => Ok(KeyCode::Media(MediaKeyCode::Record)),
            "<volup>" => Ok(KeyCode::Media(MediaKeyCode::RaiseVolume)),
            "<voldown>" => Ok(KeyCode::Media(MediaKeyCode::LowerVolume)),
            "<mute>" => Ok(KeyCode::Media(MediaKeyCode::MuteVolume)),
            "<lshift>" => Ok(KeyCode::Modifier(ModifierKeyCode::LeftShift)),
            "<lctrl>" => Ok(KeyCode::Modifier(ModifierKeyCode::LeftControl)),
            "<lalt>" => Ok(KeyCode::Modifier(ModifierKeyCode::LeftAlt)),
            "<lsuper>" => Ok(KeyCode::Modifier(ModifierKeyCode::LeftSuper)),
            "<lhyper>" => Ok(KeyCode::Modifier(ModifierKeyCode::LeftHyper)),
            "<lmeta>" => Ok(KeyCode::Modifier(ModifierKeyCode::LeftMeta)),
            "<rshift>" => Ok(KeyCode::Modifier(ModifierKeyCode::RightShift)),
            "<rctrl>" => Ok(KeyCode::Modifier(ModifierKeyCode::RightControl)),
            "<ralt>" => Ok(KeyCode::Modifier(ModifierKeyCode::RightAlt)),
            "<rsuper>" => Ok(KeyCode::Modifier(ModifierKeyCode::RightSuper)),
            "<rhyper>" => Ok(KeyCode::Modifier(ModifierKeyCode::RightHyper)),
            "<rmeta>" => Ok(KeyCode::Modifier(ModifierKeyCode::RightMeta)),
            "<lv3shift>" => Ok(KeyCode::Modifier(ModifierKeyCode::IsoLevel3Shift)),
            "<lv5shift>" => Ok(KeyCode::Modifier(ModifierKeyCode::IsoLevel5Shift)),
            "<f1>" => Ok(KeyCode::F(1)),
            "<f2>" => Ok(KeyCode::F(2)),
            "<f3>" => Ok(KeyCode::F(3)),
            "<f4>" => Ok(KeyCode::F(4)),
            "<f5>" => Ok(KeyCode::F(5)),
            "<f6>" => Ok(KeyCode::F(6)),
            "<f7>" => Ok(KeyCode::F(7)),
            "<f8>" => Ok(KeyCode::F(8)),
            "<f9>" => Ok(KeyCode::F(9)),
            "<f10>" => Ok(KeyCode::F(10)),
            "<f11>" => Ok(KeyCode::F(11)),
            "<f12>" => Ok(KeyCode::F(12)),
            ch if from.chars().count() == 1 => Ok(KeyCode::Char(ch.chars().next().unwrap())),
            _ => Err(Error {
                kind: ErrorKind::Parse,
                message: format!("invalid key"),
                severity: Severity::Warn,
            }),
        }
    }

    fn to_string(&self) -> String {
        match self {
            KeyCode::Backspace => String::from("<backspace>"),
            KeyCode::Enter => String::from("<enter>"),
            KeyCode::Left => String::from("<left>"),
            KeyCode::Right => String::from("<right>"),
            KeyCode::Up => String::from("<up>"),
            KeyCode::Down => String::from("<down>"),
            KeyCode::Home => String::from("<home>"),
            KeyCode::End => String::from("<end>"),
            KeyCode::PageUp => String::from("<pageup>"),
            KeyCode::PageDown => String::from("<pagedown>"),
            KeyCode::Tab => String::from("<tab>"),
            KeyCode::BackTab => String::from("<backtab>"),
            KeyCode::Delete => String::from("<delete>"),
            KeyCode::Insert => String::from("<insert>"),
            KeyCode::Null => String::from("<null>"),
            KeyCode::Esc => String::from("<esc>"),
            KeyCode::F(n) => format!("<f{}>", n),
            KeyCode::Char(ch) => ch.to_string(),
            KeyCode::CapsLock => String::from("<capslock>"),
            KeyCode::ScrollLock => String::from("<scrolllock>"),
            KeyCode::NumLock => String::from("<numlock>"),
            KeyCode::PrintScreen => String::from("<prtsc>"),
            KeyCode::Pause => String::from("<break>"),
            KeyCode::Menu => String::from("<menu>"),
            KeyCode::KeypadBegin => String::from("<begin>"),
            KeyCode::Media(media) => match media {
                MediaKeyCode::Play => String::from("<play>"),
                MediaKeyCode::Pause => String::from("<pause>"),
                MediaKeyCode::PlayPause => String::from("<playpause>"),
                MediaKeyCode::Reverse => String::from("<reverse>"),
                MediaKeyCode::Stop => String::from("<stop>"),
                MediaKeyCode::FastForward => String::from("<fastforward>"),
                MediaKeyCode::Rewind => String::from("<rewind>"),
                MediaKeyCode::TrackNext => String::from("<next>"),
                MediaKeyCode::TrackPrevious => String::from("<prev>"),
                MediaKeyCode::Record => String::from("<record>"),
                MediaKeyCode::LowerVolume => String::from("<volup>"),
                MediaKeyCode::RaiseVolume => String::from("<voldown>"),
                MediaKeyCode::MuteVolume => String::from("<mute>"),
            },
            KeyCode::Modifier(modifier) => match modifier {
                ModifierKeyCode::LeftShift => String::from("<lshift>"),
                ModifierKeyCode::LeftControl => String::from("<lctrl>"),
                ModifierKeyCode::LeftAlt => String::from("<lalt>"),
                ModifierKeyCode::LeftSuper => String::from("<lsuper>"),
                ModifierKeyCode::LeftHyper => String::from("<lhyper>"),
                ModifierKeyCode::LeftMeta => String::from("<lmeta>"),
                ModifierKeyCode::RightShift => String::from("<rshift>"),
                ModifierKeyCode::RightControl => String::from("<rctrl>"),
                ModifierKeyCode::RightAlt => String::from("<ralt>"),
                ModifierKeyCode::RightSuper => String::from("<rsuper>"),
                ModifierKeyCode::RightHyper => String::from("<rhyper>"),
                ModifierKeyCode::RightMeta => String::from("<rmeta>"),
                ModifierKeyCode::IsoLevel3Shift => String::from("<lv3shift>"),
                ModifierKeyCode::IsoLevel5Shift => String::from("<lv5shift>"),
            },
        }
    }
}

impl StringExt for Color {
    fn from_str(from: &str) -> Result<Self, Error>
    where
        Self: Sized,
    {
        lazy_static! {
            static ref RE_RGB: Regex = Regex::new(r"#([0-9a-f]{2}){3}").unwrap();
        }
        match from.to_lowercase().as_str() {
            "black" => Ok(Color::Black),
            "red" => Ok(Color::Red),
            "green" => Ok(Color::Green),
            "yellow" => Ok(Color::Yellow),
            "blue" => Ok(Color::Blue),
            "magenta" => Ok(Color::Magenta),
            "cyan" => Ok(Color::Cyan),
            "gray" => Ok(Color::Gray),
            "dark-gray" => Ok(Color::DarkGray),
            "lignt-red" => Ok(Color::LightRed),
            "light-green" => Ok(Color::LightGreen),
            "light-yellow" => Ok(Color::LightYellow),
            "light-blue" => Ok(Color::LightBlue),
            "light-magenta" => Ok(Color::LightMagenta),
            "light-cyan" => Ok(Color::LightCyan),
            "white" => Ok(Color::White),
            rgb => {
                if let Some(cap) = RE_RGB.captures(rgb) {
                    let (r, g, b) = (
                        u8::from_str_radix(cap.get(1).unwrap().as_str(), 16).unwrap(),
                        u8::from_str_radix(cap.get(2).unwrap().as_str(), 16).unwrap(),
                        u8::from_str_radix(cap.get(3).unwrap().as_str(), 16).unwrap(),
                    );
                    Ok(Color::Rgb(r, g, b))
                } else {
                    Err(Error {
                        kind: ErrorKind::Parse,
                        message: format!("invalid color"),
                        severity: Severity::Warn,
                    })
                }
            }
        }
    }

    fn to_string(&self) -> String {
        match self {
            Color::Black => format!("black"),
            Color::Red => format!("red"),
            Color::Green => format!("green"),
            Color::Yellow => format!("yellow"),
            Color::Blue => format!("blue"),
            Color::Magenta => format!("magenta"),
            Color::Cyan => format!("cyan"),
            Color::Gray => format!("gray"),
            Color::DarkGray => format!("dark-gray"),
            Color::LightRed => format!("light-red"),
            Color::LightGreen => format!("light-green"),
            Color::LightYellow => format!("light-yellow"),
            Color::LightBlue => format!("light-blue"),
            Color::LightMagenta => format!("light-magenta"),
            Color::LightCyan => format!("light-cyan"),
            Color::White => format!("white"),
            Color::Rgb(r, g, b) => format!("#{:02x?}{:02x?}{:02x?}", r, g, b),
            Color::Reset => format!("reset"),
            Color::Indexed(idx) => format!("[{}]", idx),
        }
    }
}

fn deserialize_color<'de, D>(deserializer: D) -> Result<Color, D::Error>
where
    D: Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    StringExt::from_str(&s)
        .map_err(|_| serde::de::Error::unknown_variant(&s, &["valid color name"]))
}

fn deserialize_option_color<'de, D>(deserializer: D) -> Result<Option<Color>, D::Error>
where
    D: Deserializer<'de>,
{
    match Deserialize::deserialize(deserializer)? {
        Some::<String>(s) => {
            Ok(Some(StringExt::from_str(&s).map_err(|_| {
                serde::de::Error::unknown_variant(&s, &["valid color name"])
            })?))
        }
        None => Ok(None),
    }
}

fn deserialize_key_map<'de, D>(deserializer: D) -> Result<Trie<KeyCode, String>, D::Error>
where
    D: Deserializer<'de>,
{
    lazy_static! {
        static ref RE_KEY: Regex = Regex::new(r"<\w+>|.").unwrap();
    }
    let hm: HashMap<String, String> = Deserialize::deserialize(deserializer)?;
    let mut trie = Trie::new();
    for (k, v) in hm {
        let key: Vec<KeyCode> = RE_KEY
            .captures_iter(&k)
            .map(|cap| {
                cap.get(0)
                    .ok_or(serde::de::Error::unknown_variant(&k, &["valid key"]))
                    .and_then(|key| {
                        KeyCode::from_str(key.as_str())
                            .map_err(|err| serde::de::Error::custom(err.to_string()))
                    })
            })
            .collect::<Result<Vec<_>, _>>()?;
        trie.try_insert(key, v)
            .map_err(|err| serde::de::Error::custom(err.to_string()))?;
    }

    Ok(trie)
}

fn deserialize_path<'de, D>(deserializer: D) -> Result<PathBuf, D::Error>
where
    D: Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    let expanded =
        shellexpand::full(&s).map_err(|err| serde::de::Error::custom(err.to_string()))?;
    Ok(PathBuf::from(expanded.as_ref()))
}
