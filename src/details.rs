/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

mod gogoanime;
mod storage;

use tui::{backend::Backend, layout::Rect, Frame};
use vibrow_core::{
    gogoanime::Kind as GogoanimeKind,
    model::{Details, Kind},
    storage::Kind as StorageKind,
};

use crate::{config::ColorPalette, utils::View};

/// Controls how details are displayed. Renders different views based on the `kind` of the details.
pub struct DetailsView {
    /// Data that should be displayed.
    pub details: Details,

    /// Set of colors that should be used in the widget.
    pub colors: ColorPalette,
}

impl View for DetailsView {
    fn render_into<B>(self, rect: &mut Frame<B>, area: Rect)
    where
        B: Backend,
    {
        match self.details.kind {
            Some(Kind::Storage(StorageKind::Directory)) => storage::DirectoryDetails {
                details: self.details,
                colors: self.colors,
            }
            .render_into(rect, area),
            Some(Kind::Gogoanime(GogoanimeKind::Anime)) => gogoanime::AnimeDetails {
                details: self.details,
                colors: self.colors,
            }
            .render_into(rect, area),
            _ => {}
        }
    }
}
