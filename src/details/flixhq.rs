/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use ratatui::{
    layout::{Constraint, Direction, Layout, Rect},
    style::{Modifier, Style},
    text::{Line, Span, Text},
    widgets::{Paragraph, Wrap},
    Frame,
};
use vibrow_core::model::Details;

use crate::{config::ColorPalette, utils::View};

/// View for the `Gogoanime::Anime` kind.
pub struct MovieDetails {
    pub details: Details,
    pub colors: ColorPalette,
}

impl View for MovieDetails {
    fn render_into(self, rect: &mut Frame, area: Rect) {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(2), Constraint::Min(0)])
            .split(area);

        let title_style = Style::default()
            .add_modifier(Modifier::BOLD)
            .fg(self.colors.accent_3);

        let label_style = Style::default()
            .add_modifier(Modifier::BOLD)
            .fg(self.colors.accent_1);

        let title =
            Paragraph::new(self.details.get_as_str("title").unwrap_or_default()).style(title_style);

        let info = {
            let mut info_text = Text::from(vec![
                Line::from(vec![
                    Span::styled("Type: ", label_style),
                    Span::styled(
                        self.details.get_as_str("type").unwrap_or_default(),
                        Style::default(),
                    ),
                ]),
                Line::from(vec![
                    Span::styled("Genres: ", label_style),
                    Span::styled(
                        self.details.get_as_str("genres").unwrap_or_default(),
                        Style::default(),
                    ),
                ]),
                Line::from(vec![
                    Span::styled("Production: ", label_style),
                    Span::styled(
                        self.details.get_as_str("production").unwrap_or_default(),
                        Style::default(),
                    ),
                ]),
                Line::from(vec![
                    Span::styled("Casts: ", label_style),
                    Span::styled(
                        self.details.get_as_str("casts").unwrap_or_default(),
                        Style::default(),
                    ),
                ]),
                Line::from(vec![
                    Span::styled("Release date: ", label_style),
                    Span::styled(
                        self.details.get_as_str("release_date").unwrap_or_default(),
                        Style::default(),
                    ),
                ]),
                Line::from(vec![
                    Span::styled("Duration: ", label_style),
                    Span::styled(
                        self.details.get_as_str("duration").unwrap_or_default(),
                        Style::default(),
                    ),
                ]),
                Line::from(vec![]),
            ]);
            info_text.extend(
                Text::from(self.details.get_as_str("description").unwrap_or_default()).into_iter(),
            );
            Paragraph::new(info_text).wrap(Wrap { trim: false })
        };

        rect.render_widget(title, chunks[0]);
        rect.render_widget(info, chunks[1]);
    }
}
