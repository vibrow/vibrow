/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use ratatui::{
    layout::{Constraint, Direction, Layout, Rect},
    style::{Modifier, Style},
    text::{Line, Span, Text},
    widgets::{Paragraph, Wrap},
    Frame,
};
use vibrow_core::model::Details;

use crate::{config::ColorPalette, utils::View};

/// View for the `Directory` kind.
pub struct DirectoryDetails {
    /// Expected to be directory data.
    pub details: Details,
    pub colors: ColorPalette,
}

impl View for DirectoryDetails {
    fn render_into(self, rect: &mut Frame, area: Rect) {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(2), Constraint::Min(0)])
            .split(area);

        let title_style = Style::default()
            .add_modifier(Modifier::BOLD)
            .fg(self.colors.accent_3);

        let label_style = Style::default()
            .add_modifier(Modifier::BOLD)
            .fg(self.colors.accent_1);

        let title =
            Paragraph::new(self.details.get_as_str("name").unwrap_or_default()).style(title_style);

        let info = Paragraph::new(Text::from(vec![
            Line::from(vec![
                Span::styled("Date Created: ", label_style),
                Span::styled(
                    self.details.get_as_str("created").unwrap_or_default(),
                    Style::default(),
                ),
            ]),
            Line::from(vec![
                Span::styled("Date Modified: ", label_style),
                Span::styled(
                    self.details.get_as_str("modified").unwrap_or_default(),
                    Style::default(),
                ),
            ]),
            Line::from(vec![
                Span::styled("Date Accessed: ", label_style),
                Span::styled(
                    self.details.get_as_str("accessed").unwrap_or_default(),
                    Style::default(),
                ),
            ]),
        ]))
        .wrap(Wrap { trim: false });

        rect.render_widget(title, chunks[0]);
        rect.render_widget(info, chunks[1]);
    }
}
