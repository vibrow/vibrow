/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use serde::{Deserialize, Serialize};
use std::{fmt, num::ParseIntError, sync::PoisonError};
use strum_macros::Display;
use vibrow_core::model::Error as DomainError;

#[derive(Debug, Clone, PartialEq, PartialOrd, Ord, Eq, Serialize, Deserialize, Display)]
#[serde(rename_all = "kebab-case")]
#[strum(serialize_all = "kebab-case")]
pub enum Severity {
    /// The most detailed log level. It is used for trace messages that may be useful for debugging
    /// or troubleshooting. These messages are typically not shown by default and are only enabled
    /// when specifically requested.
    Trace,

    /// This log level is used to provide additional details for debugging purposes. It is used for
    /// messages that contain useful information for developers to diagnose and identify any issues
    /// in the code
    Debug,

    /// This log level is used to provide information about the application's runtime behavior.
    Info,

    /// This log level is used to indicate that something unusual or unexpected happened, but that
    /// the application is still running normally. It is used for warnings that may not be critical
    /// errors but could benefit from further investigation.
    Warn,

    /// This log level is used to indicate that an error has occurred that affects the application's
    /// functionality or performance. It is used for errors that may be recoverable, as well as for
    /// those that are not.
    Error,

    /// The most critical log level. It is used to indicate that the application has encountered a
    /// severe error that has caused it to stop working properly and can no longer function. Fatal
    /// messages are often used for unrecoverable errors or exceptions that cause the application
    /// to shut down.
    Fatal,
}

/// Enumeration of possible error types in the application.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ErrorKind {
    /// This error occurs when manipulating string. Since strings are utf-8 encoded in rust, indices
    /// don't necessarily match the characters, i.e. index `n` doesn't always match the `n`-th
    /// character. In such cases this error is raised instead of panicking.
    IndexNotOnCharBoundary,

    /// This error occurs when trying to access an element of an iterator that is out of range.
    IndexOutOfRange,

    /// This error occurs when a function doesn't make any changes. For example, when the last item
    /// in the list is selected and user tries to select the next item, no changes will be made and
    /// this error will be returned.
    NoChange,

    /// This error occurs when a function requires that an item is selected, but selection is empty.
    NotSelected,

    /// This error occurs when multiple items are selected and a function requires that a single
    /// item is selected.
    MultipleItemsSelected,

    /// This error occurs when requested data is not yet loaded.
    NotLoaded,

    /// This error occurs when mutex is poisoned.
    Poisoned,

    /// This error occurs when a command or keybind is not defined.
    Undefined,

    /// This error occurs when data can't be parsed.
    Parse,

    /// This error occurs when item with given properties is not in the list.
    NotFound,

    /// This represents IO error
    IOError(std::io::ErrorKind),

    /// This is a class of errors that occur in the domain layer.
    Domain,
}

/// A top-level error in the application. It contains kind of the error, message and severity.
#[derive(Debug, Clone, thiserror::Error)]
pub struct Error {
    /// Kind of the error
    pub kind: ErrorKind,

    /// Error message
    pub message: String,

    /// Severity of the error
    pub severity: Severity,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl From<DomainError> for Error {
    fn from(err: DomainError) -> Self {
        Error {
            kind: ErrorKind::Domain,
            message: err.to_string(),
            severity: Severity::Warn,
        }
    }
}

impl<T> From<PoisonError<T>> for Error {
    fn from(err: PoisonError<T>) -> Self {
        Error {
            kind: ErrorKind::Poisoned,
            message: err.to_string(),
            severity: Severity::Fatal,
        }
    }
}

impl From<strum::ParseError> for Error {
    fn from(err: strum::ParseError) -> Self {
        Error {
            kind: ErrorKind::Parse,
            message: err.to_string(),
            severity: Severity::Warn,
        }
    }
}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Self {
        Error {
            kind: ErrorKind::Parse,
            message: err.to_string(),
            severity: Severity::Warn,
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Error {
            kind: ErrorKind::IOError(err.kind()),
            message: err.to_string(),
            severity: Severity::Warn,
        }
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Self {
        Error {
            kind: ErrorKind::Parse,
            message: err.to_string(),
            severity: Severity::Warn,
        }
    }
}
