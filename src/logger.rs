/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    fmt::{Display, Formatter, Result as FmtResult},
    fs::OpenOptions,
    io::Write,
    path::{Path, PathBuf},
};

use chrono::{DateTime, Local};
use ratatui::{
    layout::Rect,
    style::{Color, Modifier, Style},
    text::{Line, Span},
    widgets::Paragraph,
    Frame,
};

use crate::{
    error::Severity,
    utils::{tea::Cmd, View},
};

/// A single log entry.
#[derive(Debug, Clone)]
pub struct Log {
    /// Timestamp for when the log was created
    pub timestamp: DateTime<Local>,
    pub severity: Severity,
    pub text: String,
}

impl Log {
    /// Creates a new log.
    pub fn new(severity: Severity, text: impl Into<String>) -> Self {
        Self {
            timestamp: Local::now(),
            severity,
            text: text.into(),
        }
    }
}

impl Display for Log {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(
            f,
            "{}, [{}] {}",
            self.timestamp.to_string(),
            self.severity.to_string(),
            self.text
        )
    }
}

/// Logger manages and contains logs.
#[derive(Debug, Clone)]
pub struct Logger {
    log: Vec<Log>,
    path: PathBuf,
    log_level: Severity,
}

impl Logger {
    /// Creates a new logger
    ///
    /// # Arguments
    /// `path` - Path to where logs should be written.
    /// `log_level` - Minimum severity of the logs that should be handled. Ignores logs with lower
    /// severity.
    pub fn new<P>(path: P, log_level: Severity) -> Self
    where
        P: AsRef<Path>,
    {
        Self {
            log: Vec::new(),
            path: path.as_ref().to_path_buf(),
            log_level,
        }
    }

    /// Creates a new log, pushes it to the collection and returns command that writes it to log
    /// file.
    pub fn log<M, S>(&mut self, severity: Severity, log: S) -> Cmd<M>
    where
        M: Send + 'static,
        S: Into<String>,
    {
        if severity >= self.log_level {
            let log = Log::new(severity, log.into());
            self.log.push(log.clone());
            let path = self.path.clone();
            Cmd::single(async move {
                let mut file = OpenOptions::new()
                    .write(true)
                    .append(true)
                    .create(true)
                    .open(&path)
                    .expect("Error opening log file.");

                file.write_all(format!("{}\n", &log).as_bytes())
                    .expect("Error writing to log file.");
                None
            })
        } else {
            Cmd::none()
        }
    }

    /// Returns path to the log file.
    pub fn path(&self) -> &Path {
        &self.path
    }

    /// Returns all logs.
    pub fn get_logs(&self) -> &Vec<Log> {
        &self.log
    }
}

/// Controls how logs are displayed in the UI.
pub struct LogView<'a> {
    /// Reference to collection of logs, usually from the logger.
    log: &'a Vec<Log>,

    /// Minimum severity of the logs that should be displayed.
    log_level: Severity,
}

impl LogView<'_> {
    /// Sets the log level
    ///
    /// # Arguments
    /// `l` - Minimum severity of the logs that should be displayed
    pub fn level(mut self, l: Severity) -> Self {
        self.log_level = l;
        self
    }
}

impl View for LogView<'_> {
    fn render_into(self, rect: &mut Frame, area: Rect) {
        let view = Paragraph::new(Line::from(
            match self
                .log
                .iter()
                .filter(|log| log.severity >= self.log_level)
                .last()
            {
                Some(log) => {
                    let style = match log.severity {
                        Severity::Trace => Style::default()
                            .fg(Color::White)
                            .add_modifier(Modifier::DIM),
                        Severity::Debug => Style::default()
                            .fg(Color::Green)
                            .add_modifier(Modifier::DIM),
                        Severity::Info => Style::default().fg(Color::Blue),
                        Severity::Warn => Style::default().fg(Color::Yellow),
                        Severity::Error => Style::default().fg(Color::Red),
                        Severity::Fatal => {
                            Style::default().fg(Color::Red).add_modifier(Modifier::BOLD)
                        }
                    };
                    vec![
                        Span::styled(format!("[{}] ", log.severity.to_string()), style),
                        Span::styled(log.text.clone(), Style::default()),
                    ]
                }
                None => vec![],
            },
        ));
        rect.render_widget(view, area);
    }
}

/// Creates a view given a logger
/// # Arguments
/// `logger` - Reference to a logger for which the view should be created:
pub fn view<'a>(logger: &'a Logger) -> LogView<'a> {
    LogView {
        log: &logger.log,
        log_level: Severity::Info,
    }
}
