/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

mod config;
mod details;
mod error;
mod logger;
mod textbox;
mod tree;
mod utils;

use std::{
    cell::RefCell,
    fmt,
    fs::{self, OpenOptions},
    io::{self, BufReader, Read, Stdout, Write},
    path::PathBuf,
    str::FromStr,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Mutex,
    },
};

use async_process::{Child, Command, ExitStatus, Stdio};
use config::StringExt;
use crossterm::event::{Event, EventStream as CrosstermEventStream, KeyCode};
use futures::{
    io::BufReader as AsyncBufReader,
    stream::{self, BoxStream},
    AsyncBufReadExt, StreamExt,
};
use lazy_static::lazy_static;
use ratatui::{
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Modifier, Style},
    text::Span,
    widgets::{Block, BorderType, Borders, Paragraph},
    Frame,
};
use regex::Regex;
use strum::{EnumCount, IntoEnumIterator};
use vibrow_core::{gogoanime, model::Provider, storage, usecases};

use crate::{
    config::{ColorPalette, Config, CACHE_DIR, DATA_DIR},
    error::{Error, ErrorKind, Severity},
    logger::Logger,
    textbox::TextBox,
    tree::{Select, Selection, Tree},
    utils::{
        tea::{Cmd, Program, Sub},
        Trie, View,
    },
};

lazy_static! {
    static ref RE_EXIT: Regex = Regex::new(r"^(exit|q)$").unwrap();
    static ref RE_INTERACTIVE: Regex = Regex::new(r"\{(.*?);(.*?)\}").unwrap();
    static ref RE_PLACEHOLDER: Regex =
        Regex::new(r"\[(?P<prefix>.*?)((?P<obj>item|cwd|log)(\.(?P<attr>\w+))?)(?P<suffix>.*?)(?P<delim>[~!@#$%^&*_=|\\:;,?/\s+-]*)\]")
            .unwrap();
    static ref RE_ITEM: Regex =
        Regex::new(r"\[item(\.(name|id|kind|path))?(.*?)([~!@#$%^&*_=|\\:;,?/\s+-]*)\]").unwrap();
    static ref RE_LINK: Regex =
        Regex::new(r"\[link(\.(\w+))?([~!@#$%^&*_=|\\:;,?/\s+-]*)\]").unwrap();
    static ref RE_CWD: Regex = Regex::new(r"\[cwd\]").unwrap();
    static ref RE_LOG: Regex = Regex::new(r"\[log\]").unwrap();
    static ref RE_SEARCH: Regex = Regex::new(r"^(search) (.*)$").unwrap();
    static ref RE_GO: Regex = Regex::new(r"^(go) (.+)$").unwrap();
    static ref RE_SHCMD: Regex = Regex::new(r"^(!)(.+)$").unwrap();
    static ref RE_FIND: Regex = Regex::new(r"^(/|find )(.+)$").unwrap();
    static ref RE_SET: Regex = Regex::new(r"^(set) (.+)=(.+)$").unwrap();
    static ref RE_TOGGLE: Regex = Regex::new(r"^(toggle) (.+)$").unwrap();
    static ref RE_YANK: Regex = Regex::new(r"^(yank)$").unwrap();
    static ref RE_CREATE_DIR: Regex = Regex::new(r"^(mkdir) (.+)$").unwrap();
    static ref RE_DELETE: Regex = Regex::new(r"^(delete)$").unwrap();
    static ref RE_PASTE: Regex = Regex::new(r"^(paste)$").unwrap();
    static ref RE_SAVE_TO: Regex = Regex::new(r"^(save) (.+?):(.+?)$").unwrap();
    static ref RE_LOAD: Regex = Regex::new(r"^(load)$").unwrap();
    static ref RE_RELOAD: Regex = Regex::new(r"^(refresh|reload)$").unwrap();
    static ref RE_RENAME: Regex = Regex::new(r"^(rename) (.+)$").unwrap();
    static ref RE_BATCH: Regex = Regex::new(r"^\[(.*)\]$").unwrap();
}

/// Describes which tree is currently focused.
#[derive(Clone, Copy, Debug)]
enum Focus {
    Left,
    Right,
}

/// Structure for the interactive mode.
#[derive(Debug, Clone)]
struct Interactive {
    /// The expression that contains at least one replacement field.
    expr: String,

    /// Prompt shown to the user.
    prompt: String,

    /// Default input.
    default: String,
}

impl Interactive {
    /// Creates new interactive struct.
    ///
    /// # Arguments
    /// `expr` - String with at least one replacement field.
    ///
    /// # Panics
    /// If `expr` doesn't contain valid replacement field.
    pub fn new<S>(expr: S) -> Self
    where
        S: Into<String>,
    {
        let expr = expr.into();
        let cap = RE_INTERACTIVE.captures(&expr).unwrap(); // runcmd already tested that it matches
        Self {
            prompt: cap
                .get(1)
                .map(|m| m.as_str())
                .unwrap_or_default()
                .to_owned(),
            default: cap
                .get(2)
                .map(|m| m.as_str())
                .unwrap_or_default()
                .to_owned(),
            expr,
        }
    }

    /// Submits the given input and produces expression with that input in place of the corresponding
    /// replacement field.
    ///
    /// # Arguments
    /// `input` - User input.
    pub fn submit<S>(&self, input: S) -> String
    where
        S: Into<String>,
    {
        RE_INTERACTIVE
            .replace(&self.expr, &input.into())
            .to_string()
    }

    /// Returns the prompt of the interactive mode.
    pub fn prompt(&self) -> &str {
        &self.prompt
    }

    /// Returns the default input.
    pub fn default(&self) -> &str {
        &self.default
    }
}

/// This represents input modes supported by the application.
#[derive(Debug, Clone, Default)]
enum Mode {
    /// The default mode where user can navigate through the application and execute commands via
    /// keystrokes
    #[default]
    Normal,

    /// Mode that allows user to provide input which will be injected into a command and then
    /// executed.
    Interactive(Interactive),
}
impl Mode {
    /// Constructor for the interactive mode
    fn interactive<S>(fstr: S) -> Self
    where
        S: Into<String>,
    {
        Self::Interactive(Interactive::new(fstr))
    }
}

/// Represents terminal and its event handler.
struct Terminal {
    inner: RefCell<ratatui::Terminal<CrosstermBackend<Stdout>>>,
    events: Mutex<CrosstermEventStream>,
}
impl Terminal {
    /// Creates new instance of terminal with raw mode enabled and hidden cursor.
    pub fn new() -> Self {
        let mut term = ratatui::Terminal::new(CrosstermBackend::new(std::io::stdout())).unwrap();
        crossterm::terminal::enable_raw_mode().unwrap();
        term.hide_cursor().unwrap();
        term.clear().unwrap();
        Self {
            inner: RefCell::new(term),
            events: Mutex::new(CrosstermEventStream::new()),
        }
    }

    /// Draws widgets. Wrapper for the [tui::Terminal::draw].
    ///
    /// # Panics
    /// If drawing fails
    pub fn draw<F>(&self, f: F)
    where
        F: FnOnce(&mut Frame<'_>),
    {
        self.inner.borrow_mut().draw(f).unwrap();
    }

    /// Returns subscription that waits for the next terminal event.
    ///
    /// # Arguments
    /// `on_success` - Function triggered on successful read of the next event that handles it and
    /// produces a message.
    ///
    /// `on_error` - Function triggered on error. It handles the error and produces a message.
    ///
    /// # Panics
    /// If stream is finished.
    pub fn on_event<'a, M, OnSuccess, OnError>(
        &'a self,
        on_success: OnSuccess,
        on_error: OnError,
    ) -> Sub<'a, M>
    where
        M: Send + 'static,
        OnSuccess: FnOnce(Event) -> M + 'a,
        OnError: FnOnce(io::Error) -> M + 'a,
    {
        Sub::single(async move {
            let mut events = self.events.lock().unwrap();
            match events
                .next()
                .await
                .unwrap() // it's ok to panic if stream is finished
            {
                Ok(evt) => on_success(evt),
                Err(err) => on_error(err),
            }
        })
    }
}
impl Drop for Terminal {
    fn drop(&mut self) {
        let mut term = self.inner.borrow_mut();
        crossterm::terminal::disable_raw_mode().unwrap();
        term.clear().unwrap();
        term.show_cursor().unwrap();
    }
}

/// Represents an async process and its IO streams.
struct Process {
    child: Child,
    stdout: Mutex<BoxStream<'static, Result<String, io::Error>>>,
    stderr: Mutex<BoxStream<'static, Result<String, io::Error>>>,
}
impl Process {
    /// Returns process id.
    pub fn id(&self) -> u32 {
        self.child.id()
    }

    /// Returns subscription that waits for the next line in the stdout.
    ///
    /// # Arguments
    /// `on_success` - Function that handles the newly read line from the stdout and produces a
    /// message
    ///
    /// `on_error` - Function that handles the error in case something goes wrong.
    ///
    /// # Panics
    /// If mutex is poisoned.
    pub fn on_stdout<'a, M, OnSuccess, OnError>(
        &'a self,
        on_success: OnSuccess,
        on_error: OnError,
    ) -> Sub<'a, M>
    where
        M: Send + 'static,
        OnSuccess: FnOnce(String) -> M + 'a,
        OnError: FnOnce(Error) -> M + 'a,
    {
        Sub::single(async move {
            let mut reader = self.stdout.lock().unwrap();
            match reader.next().await {
                Some(Ok(line)) => on_success(line.trim().to_string()),
                Some(Err(err)) => on_error(Error::from(err)),
                None => on_error(Error::from(io::Error::new(
                    io::ErrorKind::BrokenPipe,
                    "process has exited",
                ))),
            }
        })
    }

    /// Returns subscription that waits for the next line in the stderr.
    ///
    /// # Arguments
    /// `on_success` - Function that handles the newly read line from the stderr and produces a
    /// message
    ///
    /// `on_error` - Function that handles the error in case something goes wrong.
    ///
    /// # Panics
    /// If mutex is poisoned.
    pub fn on_stderr<'a, M, OnSuccess, OnError>(
        &'a self,
        on_success: OnSuccess,
        on_error: OnError,
    ) -> Sub<'a, M>
    where
        M: Send + 'static,
        OnSuccess: FnOnce(String) -> M + 'a,
        OnError: FnOnce(Error) -> M + 'a,
    {
        Sub::single(async move {
            let mut reader = self.stderr.lock().unwrap();
            match reader.next().await {
                Some(Ok(line)) => on_success(line.trim().to_string()),
                Some(Err(err)) => on_error(Error::from(err)),
                None => on_error(Error::from(io::Error::new(
                    io::ErrorKind::BrokenPipe,
                    "process has exited",
                ))),
            }
        })
    }

    /// Returns command that waits for the process to finish, handles the exit status and optionally
    /// returns a message. This is a command, rather than subscription, because it requires mutable
    /// access and it is one-shot per process, unlike subscriptions that are continually observing
    /// some changes.
    ///
    /// # Arguments
    /// `on_success` - Function that handles exit status on successful exit.
    ///
    /// `on_error` - Function that handles the error in case process exits abnormally.
    pub fn wait<M, OnSuccess, OnError>(
        &mut self,
        on_success: OnSuccess,
        on_error: OnError,
    ) -> Cmd<M>
    where
        M: Send + 'static,
        OnSuccess: FnOnce(ExitStatus) -> Option<M> + Send + 'static,
        OnError: FnOnce(Error) -> Option<M> + Send + 'static,
    {
        let status = self.child.status();
        Cmd::single(async move {
            match status.await {
                Ok(status) => on_success(status),
                Err(err) => on_error(Error::from(err)),
            }
        })
    }
}
impl Drop for Process {
    fn drop(&mut self) {
        self.child.kill().ok();
    }
}
impl From<Child> for Process {
    fn from(mut child: Child) -> Self {
        let stdout = Mutex::new(
            AsyncBufReader::new(child.stdout.take().unwrap())
                .lines()
                .boxed(),
        );
        let stderr = Mutex::new(
            AsyncBufReader::new(child.stderr.take().unwrap())
                .lines()
                .boxed(),
        );
        Self {
            child,
            stdout,
            stderr,
        }
    }
}
impl fmt::Debug for Process {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.id())
    }
}

/// Parses the string expression and returns corresponding message.
///
/// # Arguments
/// `expr` - User provided expression that should be parsed into a message.
///
/// # Errors
/// - If the expression doesn't match any valid command.
/// - If command is valid, but the arguments passed to it are not.
fn parse_expression<S>(expr: S) -> Result<Msg, Error>
where
    S: AsRef<str>,
{
    const FATAL_REGEX_GROUP_OUT_OF_RANGE: &'static str =
        "Index out of range for regex capture group.";

    fn err_undefined(expr: &str) -> Error {
        Error {
            kind: ErrorKind::Undefined,
            message: format!("unknown command `{}`", expr),
            severity: Severity::Warn,
        }
    }

    let expr = expr.as_ref();

    if RE_PLACEHOLDER.is_match(expr) {
        Ok(Msg::ReplacePlaceholder(expr.to_string()))
    } else if RE_INTERACTIVE.is_match(expr) {
        Ok(Msg::SetMode(Mode::interactive(expr)))
    } else if RE_EXIT.is_match(expr) {
        Ok(Msg::Exit)
    } else if let Some(cap) = RE_SHCMD.captures(expr) {
        let cmd_str = cap.get(2).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE).as_str();
        Ok(Msg::RunShCmd(cmd_str.to_string()))
    } else if let Some(cap) = RE_FIND.captures(expr) {
        let query = cap.get(2).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE).as_str();
        Ok(Msg::Find(query.to_string()))
    } else if let Some(cap) = RE_CREATE_DIR.captures(expr) {
        let name = cap.get(2).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE).as_str();
        Ok(Msg::CreateDir(name.to_string()))
    } else if RE_YANK.is_match(expr) {
        Ok(Msg::Yank)
    } else if RE_DELETE.is_match(expr) {
        Ok(Msg::Delete)
    } else if RE_PASTE.is_match(expr) {
        Ok(Msg::Paste)
    } else if RE_LOAD.is_match(expr) {
        Ok(Msg::LoadMore)
    } else if let Some(cap) = RE_SAVE_TO.captures(expr) {
        let provider =
            Provider::from_str(cap.get(2).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE).as_str())?;
        let dst = PathBuf::from(cap.get(3).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE).as_str());
        Ok(Msg::SaveTo(provider, dst))
    } else if RE_RELOAD.is_match(expr) {
        Ok(Msg::ReloadFocused)
    } else if let Some(cap) = RE_SEARCH.captures(expr) {
        let query = cap
            .get(2)
            .expect(FATAL_REGEX_GROUP_OUT_OF_RANGE)
            .as_str()
            .to_owned();
        Ok(Msg::Search(query))
    } else if let Some(cap) = RE_RENAME.captures(expr) {
        let new_name = cap
            .get(2)
            .expect(FATAL_REGEX_GROUP_OUT_OF_RANGE)
            .as_str()
            .to_owned();
        Ok(Msg::Rename(new_name))
    } else if let Some(cap) = RE_BATCH.captures(expr) {
        Ok(Msg::Batch(
            cap.get(1)
                .expect(FATAL_REGEX_GROUP_OUT_OF_RANGE)
                .as_str()
                .split(",")
                .map(|word| parse_expression(word.trim()))
                .collect::<Result<Vec<_>, _>>()?,
        ))
    } else if let Some(cap) = RE_TOGGLE.captures(expr) {
        match cap.get(2).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE).as_str() {
            "mode" => Ok(Msg::ToggleVisualMode),
            "focus" => Ok(Msg::ToggleFocus),
            "quickview" => Ok(Msg::ToggleQuickView),
            cmd => Err(err_undefined(cmd)),
        }
    } else if let Some(cap) = RE_GO.captures(expr) {
        match cap.get(2).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE).as_str() {
            "up" => Ok(Msg::SelectPreviousItem),
            "down" => Ok(Msg::SelectNextItem),
            "into" => Ok(Msg::Enter),
            "out" => Ok(Msg::Leave),
            "top" => Ok(Msg::SelectFirstItem),
            "bottom" => Ok(Msg::SelectLastItem),
            idx_str => Ok(Msg::SelectFocused(Some(
                idx_str.parse::<usize>()?.saturating_sub(1),
            ))),
        }
    } else if let Some(cap) = RE_SET.captures(expr) {
        let key = cap.get(2).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE);
        let val = cap.get(3).expect(FATAL_REGEX_GROUP_OUT_OF_RANGE);
        match key.as_str() {
            "mode" => match val.as_str() {
                "normal" => Ok(Msg::SetMode(Mode::Normal)),
                "visual" => Ok(Msg::SetVisual(true)),
                cmd => Err(err_undefined(cmd)),
            },
            "focus" => match val.as_str() {
                "left" => Ok(Msg::SetFocus(Focus::Left)),
                "right" => Ok(Msg::SetFocus(Focus::Right)),
                cmd => Err(err_undefined(cmd)),
            },
            "quickview" => match val.as_str() {
                "on" => Ok(Msg::SetQuickView(true)),
                "off" => Ok(Msg::SetQuickView(false)),
                cmd => Err(err_undefined(cmd)),
            },
            "provider" => match val.as_str() {
                "next" => Ok(Msg::NextProvider),
                "prev" | "previous" => Ok(Msg::PreviousProvider),
                provider => Ok(Msg::SetProvider(Provider::from_str(provider)?)),
            },
            cmd => Err(err_undefined(cmd)),
        }
    } else {
        Err(err_undefined(expr))
    }
}

/// Controls how keystrokes are displayed to the user.
struct KeystrokeView<'a> {
    pub strokes: &'a Vec<KeyCode>,
}
impl View for KeystrokeView<'_> {
    fn render_into(self, rect: &mut Frame, area: Rect) {
        let strokes_str = self
            .strokes
            .iter()
            .map(StringExt::to_string)
            .fold(String::new(), |acc, k| format!("{acc}{k}-"));

        let style = Style::default();
        let view =
            Paragraph::new(Span::styled(strokes_str, style)).block(Block::default().style(style));
        rect.render_widget(view, area);
    }
}

/// Represents the current state of the application. It can only be updated through an [update]
/// function and read by the [view] and [subscriptions].
struct Model {
    /// Title of the application.
    title: &'static str,

    /// Set of colors used by the widgets.
    colors: ColorPalette,

    /// Terminal and its event handler.
    terminal: Terminal,

    /// Current input mode.
    mode: Mode,

    /// Mapping between keystrokes and corresponding commands.
    keymap: Trie<KeyCode, String>,

    /// Buffer for the keystrokes. Contains currently typed keystrokes.
    cmd_buff: Vec<KeyCode>,

    /// Textbox used for the interactive mode.
    text_box: TextBox,

    /// Trees for every supported provider. Each tree represents current state for the particular
    /// provider, such as loaded items, selection, etc.
    trees: [Tree; Provider::COUNT],

    /// Currently active providers, since the application can display only two of them at a time.
    active: [Provider; 2],

    /// Currently focused of the two blocks.
    focus: Focus,

    /// When quick view is active, only the focused tree is displayed and the other block is used to
    /// display the details of the selected item.
    quick_view: bool,

    /// List of currently running processes.
    procs: Vec<Process>,

    logger: Logger,
    is_running: Arc<AtomicBool>,
}

impl Model {
    #[inline]
    pub fn focused_provider(&self) -> Provider {
        match self.focus {
            Focus::Left => self.active[0],
            Focus::Right => self.active[1],
        }
    }

    #[inline]
    pub fn left_tree(&self) -> &Tree {
        &self.trees[self.active[0] as usize]
    }

    #[inline]
    pub fn left_tree_mut(&mut self) -> &mut Tree {
        &mut self.trees[self.active[0] as usize]
    }

    #[inline]
    pub fn right_tree(&self) -> &Tree {
        &self.trees[self.active[1] as usize]
    }

    #[inline]
    pub fn right_tree_mut(&mut self) -> &mut Tree {
        &mut self.trees[self.active[1] as usize]
    }

    #[inline]
    pub fn focused_tree(&self) -> &Tree {
        match self.focus {
            Focus::Left => self.left_tree(),
            Focus::Right => self.right_tree(),
        }
    }

    #[inline]
    pub fn focused_tree_mut(&mut self) -> &mut Tree {
        match self.focus {
            Focus::Left => self.left_tree_mut(),
            Focus::Right => self.right_tree_mut(),
        }
    }
}

/// Function that draws all the widgets.
fn view(model: &Model) {
    model.terminal.draw(|rect| {
        let block_style = {
            let mut style = Style::default();
            style.bg = model.colors.background;
            style.fg = model.colors.foreground;
            style
        };
        rect.render_widget(Block::default().style(block_style), rect.size());

        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                Constraint::Length(3),
                Constraint::Min(2),
                Constraint::Length(1),
            ])
            .split(rect.size());

        let body_chunks = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
            .split(chunks[1]);

        let title_block = Block::default()
            .borders(Borders::ALL)
            .border_type(BorderType::Plain)
            .style(block_style);
        let title_block_area = title_block.inner(chunks[0]);
        rect.render_widget(title_block, chunks[0]);

        let left_block_style = match (model.quick_view, model.focus) {
            (false, Focus::Right) => block_style.add_modifier(Modifier::DIM),
            _ => block_style,
        };
        let left_block = Block::default()
            .borders(Borders::ALL)
            .border_type(BorderType::Plain)
            .title(match (model.quick_view, model.focus) {
                (true, Focus::Right) => String::new(),
                _ => match model.left_tree().path() {
                    Ok(path) => {
                        format!("{}/{}", model.active[0], path.to_str().unwrap_or_default())
                    }
                    Err(_) => model.active[0].to_string(),
                },
            })
            .style(left_block_style);
        let left_block_area = left_block.inner(body_chunks[0]);
        rect.render_widget(left_block, body_chunks[0]);

        let right_block_style = match (model.quick_view, model.focus) {
            (false, Focus::Left) => block_style.add_modifier(Modifier::DIM),
            _ => block_style,
        };
        let right_block = Block::default()
            .borders(Borders::ALL)
            .border_type(BorderType::Plain)
            .title(match (model.quick_view, model.focus) {
                (true, Focus::Left) => String::new(),
                _ => match model.right_tree().path() {
                    Ok(path) => {
                        format!("{}/{}", model.active[1], path.to_str().unwrap_or_default())
                    }
                    Err(_) => model.active[1].to_string(),
                },
            })
            .style(right_block_style);
        let right_block_area = right_block.inner(body_chunks[1]);
        rect.render_widget(right_block, body_chunks[1]);

        let footer_block = Block::default().style(block_style);
        let footer_block_area = footer_block.inner(chunks[2]);
        rect.render_widget(footer_block, chunks[2]);

        // Render title
        rect.render_widget(
            Paragraph::new(model.title)
                .style(block_style)
                .alignment(Alignment::Center),
            title_block_area,
        );

        // Render body
        match (model.focus, model.quick_view) {
            (_focus, false) => {
                let (left_tree, _) = tree::view(model.left_tree());
                let (right_tree, _) = tree::view(model.right_tree());
                left_tree.render_into(rect, left_block_area);
                right_tree.render_into(rect, right_block_area);
            }
            (Focus::Left, true) => {
                let (left_tree, left_details) = tree::view(model.left_tree());
                left_tree.render_into(rect, left_block_area);
                left_details.render_into(rect, right_block_area);
            }
            (Focus::Right, true) => {
                let (right_tree, right_details) = tree::view(model.right_tree());
                right_details.render_into(rect, left_block_area);
                right_tree.render_into(rect, right_block_area);
            }
        }

        // Render footer
        if let Mode::Interactive(interactive) = &model.mode {
            let prompt = format!("{}", interactive.prompt());
            let footer_chunks = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Length(prompt.len() as u16), Constraint::Min(1)])
                .split(footer_block_area);
            let prompt_view = Paragraph::new(Span::styled(
                prompt,
                Style::default().fg(model.colors.accent_1),
            ));
            rect.render_widget(prompt_view, footer_chunks[0]);
            textbox::view(&model.text_box).render_into(rect, footer_chunks[1]);
        } else if !model.cmd_buff.is_empty() {
            KeystrokeView {
                strokes: &model.cmd_buff,
            }
            .render_into(rect, footer_block_area);
        } else {
            logger::view(&model.logger)
                .level(Severity::Info)
                .render_into(rect, footer_block_area);
        }
    })
}

/////////////////////////////////////////
// UPDATE

/// Enumeration of all possible operations that application can perform. It is used for communication
/// between different components of the architecture. [update] is the only function that handles
/// these messages to update the model and run commands. [view] and [subscriptions] can emit them.
#[derive(Debug)]
enum Msg {
    /// Exits the application.
    Exit,

    /// Do nothing. Used to refresh the view.
    Tick,

    /// Logs a message.
    Log(Severity, String),

    /// Moves cursor to the given position.
    MoveCursor(u16),

    /// Handles key press events.
    KeyPress(KeyCode),

    /// Focuses given block.
    SetFocus(Focus),

    /// Enables or disables quick view.
    SetQuickView(bool),

    /// Sets given provider to the currently focused block.
    SetProvider(Provider),

    /// Enables or disables visual mode, which determines if single or multiple items are selected.
    SetVisual(bool),

    /// Sets input mode.
    SetMode(Mode),

    /// Toggles focus between left and right.
    ToggleFocus,

    /// Toggles quick view.
    ToggleQuickView,

    /// Toggles visual mode.
    ToggleVisualMode,

    /// Selects provided index to the provider. In rare cases it is necessary to change selection for
    /// not focused providers. One of these cases is during [Msg::SaveTo] which triggers
    /// [Msg::Reload] which re-selects the previously selected item, and all these operations should
    /// happen on an unfocused block.
    Select(Provider, Option<usize>),

    /// Selects provided index to the focused provider.
    SelectFocused(Option<usize>),

    /// Forces select, even if the given index is already selected. This is necessary in situations
    /// when selected index is the same, but different item appears on that index, e.g. after delete.
    ForceSelect(Provider, Option<usize>),

    /// Selects the item following the currently selected one.
    SelectNextItem,

    /// Selects the item preceding the currently selected one.
    SelectPreviousItem,

    /// Selects the first item in the list.
    SelectFirstItem,

    /// Selects the last currently loaded item in the list.
    SelectLastItem,

    /// Enters the selected item, loading its contents.
    Enter,

    /// Leaves the current node, going back to the previous one.
    Leave,

    /// Cycles providers backwards.
    PreviousProvider,

    /// Cycles providers forwards.
    NextProvider,

    /// Searches for an item that contains the query and selects it.
    Find(String),

    /// Copys all selected items to the clipboard.
    Yank,

    /// Pastes items that are currently in the clipboard to the current location.
    Paste,

    /// Moves all selected items to the clipboard. They are permanently deleted only after the
    /// clipboard is overwritten.
    Delete,

    /// Saves currently selected items directly to the given location.
    SaveTo(Provider, PathBuf),

    /// Creates directory in the current location.
    CreateDir(String),

    /// Renames currently selected item. It's possible to rename only one item at a time.
    Rename(String),

    /// Sends search request to the currently focused provider.
    Search(String),

    /// Sends request to the currently focused provider to load next page.
    LoadMore,

    /// Reloads all of the currently loaded pages for the given provider.
    Reload(Provider),

    /// Reloads all of the currently loaded pages for the currently focused provider.
    ReloadFocused,

    /// Loads details for the selected item. This message is triggered by the subscription.
    LoadDetails,

    /// Runs shell command as an async process.
    RunShCmd(String),

    /// Adds the given process to the list.
    AddProcess(Process),

    /// Removes process with the given id from the list, killing it.
    RemoveProcess(u32),

    /// Parses the expression and replaces corresponding format fields.
    ReplacePlaceholder(String),

    /// Submits user's input.
    Submit,

    /// Runs multiple messages at once.
    Batch(Vec<Msg>),
}

/// Updates the model based on the message.
fn try_update(msg: Msg, model: &mut Model) -> Result<Cmd<Msg>, Error> {
    Ok(match msg {
        Msg::Exit => {
            model.is_running.store(false, Ordering::Relaxed);
            Cmd::none()
        }
        Msg::Tick => Cmd::none(),
        Msg::Log(severity, log) => model.logger.log(severity, log),
        Msg::MoveCursor(pos) => {
            model.text_box.move_cursor(pos)?;
            Cmd::none()
        }
        Msg::KeyPress(key) => match model.mode {
            Mode::Normal => {
                model.cmd_buff.push(key);
                match model.keymap.get(&model.cmd_buff) {
                    None => {
                        let text = model
                            .cmd_buff
                            .iter()
                            .map(StringExt::to_string)
                            .collect::<Vec<_>>()
                            .join("-");
                        model.cmd_buff.clear();

                        Err(Error {
                            kind: ErrorKind::Undefined,
                            message: format!("{} is undefined", text),
                            severity: Severity::Info,
                        })?
                    }
                    Some(Trie::Node(_children)) => Cmd::none(),
                    Some(Trie::Leaf(expr)) => {
                        model.cmd_buff.clear();
                        let msg = parse_expression(expr)?;
                        update(msg, model)
                    }
                }
            }
            Mode::Interactive(_) => match key {
                KeyCode::Enter => update(Msg::Submit, model),
                KeyCode::Esc => update(Msg::SetMode(Mode::Normal), model),
                KeyCode::Backspace => match model.text_box.backward_delete_char() {
                    Err(err) => match err.kind {
                        ErrorKind::NoChange => update(Msg::SetMode(Mode::Normal), model),
                        _ => Err(err)?,
                    },
                    Ok(_) => Cmd::none(),
                },
                KeyCode::Delete => match model.text_box.forward_delete_char() {
                    Err(err) => match err.kind {
                        ErrorKind::NoChange => update(Msg::SetMode(Mode::Normal), model),
                        _ => Err(err)?,
                    },
                    Ok(_) => Cmd::none(),
                },
                KeyCode::Char(c) => {
                    model.text_box.insert_char(c)?;
                    Cmd::none()
                }
                KeyCode::Left => {
                    let pos = model.text_box.cursor_position().saturating_sub(1);
                    update(Msg::MoveCursor(pos), model)
                }
                KeyCode::Right => {
                    let pos = model.text_box.cursor_position().saturating_add(1);
                    update(Msg::MoveCursor(pos), model)
                }
                KeyCode::Home => {
                    let pos = 0;
                    update(Msg::MoveCursor(pos), model)
                }
                KeyCode::End => {
                    let pos = model.text_box.get_text().chars().count() as u16;
                    update(Msg::MoveCursor(pos), model)
                }
                _ => Cmd::none(),
            },
        },
        Msg::SetFocus(focus) => {
            model.focus = focus;
            Cmd::none()
        }
        Msg::SetQuickView(b) => {
            model.quick_view = b;
            Cmd::none()
        }
        Msg::SetProvider(provider) => {
            match model.focus {
                Focus::Left => model.active[0] = provider,
                Focus::Right => model.active[1] = provider,
            };
            Cmd::none()
        }
        Msg::SetVisual(b) => {
            if b {
                model
                    .focused_tree_mut()
                    .current_node
                    .switch_to_multiple_selection()?;
                update(Msg::Log(Severity::Info, format!("visual model on")), model)
            } else {
                model
                    .focused_tree_mut()
                    .current_node
                    .switch_to_single_selection()?;
                update(Msg::Log(Severity::Info, format!("visual model off")), model)
            }
        }
        Msg::SetMode(mode) => {
            match mode {
                Mode::Normal => {
                    model.text_box.set_text("");
                    model.mode = Mode::Normal;
                }
                Mode::Interactive(interactive) => {
                    model.text_box.set_text(interactive.default());
                    model.mode = Mode::Interactive(interactive);
                }
            };
            Cmd::none()
        }
        Msg::ToggleFocus => {
            match model.focus {
                Focus::Left => model.focus = Focus::Right,
                Focus::Right => model.focus = Focus::Left,
            };
            Cmd::none()
        }
        Msg::ToggleQuickView => {
            model.quick_view = !model.quick_view;
            Cmd::none()
        }
        Msg::ToggleVisualMode => {
            let selection = model.focused_tree_mut().current_node.selection();
            if selection.is_single() {
                model
                    .focused_tree_mut()
                    .current_node
                    .switch_to_multiple_selection()?;
                update(Msg::Log(Severity::Info, format!("visual model on")), model)
            } else if selection.is_multiple() {
                model
                    .focused_tree_mut()
                    .current_node
                    .switch_to_single_selection()?;
                update(Msg::Log(Severity::Info, format!("visual model off")), model)
            } else {
                Cmd::none()
            }
        }
        Msg::Select(provider, idx) => {
            let idx = model.trees[provider as usize]
                .current_node
                .select(idx, Select::IfDifferent)?;
            update(
                Msg::Log(Severity::Debug, format!("selected {:?}", idx)),
                model,
            )
        }
        Msg::SelectFocused(idx) => update(Msg::Select(model.focused_provider(), idx), model),
        Msg::ForceSelect(provider, idx) => {
            let idx = model.trees[provider as usize]
                .current_node
                .select(idx, Select::Always)?;
            update(
                Msg::Log(Severity::Debug, format!("selected {:?}", idx)),
                model,
            )
        }
        Msg::SelectNextItem => {
            let node = &mut model.focused_tree_mut().current_node;
            match node.selection() {
                Selection::None => update(Msg::SelectFirstItem, model),
                Selection::Single(selected) | Selection::Multiple(_, selected) => {
                    let idx = selected.checked_add(1);
                    if idx >= Some(node.items().len())
                        && node.has_more_pages()
                        && !node.is_loading()
                    {
                        update(Msg::LoadMore, model)
                    } else {
                        update(Msg::SelectFocused(idx), model)
                    }
                }
            }
        }
        Msg::SelectPreviousItem => match model.focused_tree_mut().current_node.selection() {
            Selection::None => update(Msg::SelectLastItem, model),
            Selection::Single(selected) | Selection::Multiple(_, selected) => {
                let idx = selected.saturating_sub(1);
                update(Msg::SelectFocused(Some(idx)), model)
            }
        },
        Msg::SelectFirstItem => update(Msg::SelectFocused(Some(0)), model),
        Msg::SelectLastItem => {
            let last = model
                .focused_tree()
                .current_node
                .items()
                .len()
                .checked_sub(1);
            update(Msg::SelectFocused(last), model)
        }
        Msg::Enter => model.focused_tree_mut().enter()?.load_next_page(
            |_page| Some(Msg::Tick),
            |err| Some(Msg::Log(err.severity, err.message)),
        ),
        Msg::Leave => {
            model.focused_tree_mut().leave()?;
            Cmd::none()
        }
        Msg::PreviousProvider => {
            match model.focus {
                Focus::Left => {
                    let pos = Provider::iter()
                        .rev()
                        .position(|p| p == model.active[0])
                        .unwrap()
                        .saturating_add(1);
                    model.active[0] = Provider::iter().rev().cycle().skip(pos).next().unwrap();
                }
                Focus::Right => {
                    let pos = Provider::iter()
                        .rev()
                        .position(|p| p == model.active[1])
                        .unwrap()
                        .saturating_add(1);
                    model.active[1] = Provider::iter().rev().cycle().skip(pos).next().unwrap();
                }
            };
            Cmd::none()
        }
        Msg::NextProvider => {
            match model.focus {
                Focus::Left => {
                    let pos = Provider::iter()
                        .position(|p| p == model.active[0])
                        .unwrap()
                        .saturating_add(1);
                    model.active[0] = Provider::iter().cycle().skip(pos).next().unwrap();
                }
                Focus::Right => {
                    let pos = Provider::iter()
                        .position(|p| p == model.active[1])
                        .unwrap()
                        .saturating_add(1);
                    model.active[1] = Provider::iter().cycle().skip(pos).cycle().next().unwrap();
                }
            };
            Cmd::none()
        }
        Msg::Find(query) => {
            let node = &mut model.focused_tree_mut().current_node;
            let start = match node.selection() {
                Selection::None => 0,
                Selection::Single(idx) => idx,
                Selection::Multiple(_, end) => end,
            };
            let idx = node
                .items()
                .iter()
                .enumerate()
                .skip(start)
                .filter(|(_, item)| (*item).name.to_lowercase().contains(&query.to_lowercase()))
                .map(|(idx, _)| idx)
                .next()
                .ok_or(Error {
                    kind: ErrorKind::NotFound,
                    message: format!("no matches for `{}`", query),
                    severity: Severity::Info,
                })?;
            update(Msg::Select(model.focused_provider(), Some(idx)), model)
        }
        Msg::Yank => {
            let items = model
                .focused_tree()
                .current_node
                .selected_items()
                .into_iter()
                .map(|(_idx, result)| result)
                .collect::<Result<Vec<_>, _>>()?;
            let len = items.len();
            Cmd::single(async move {
                match usecases::copy_to_clipboard(Provider::Storage, items)
                    .await
                    .map_err(Error::from)
                {
                    Ok(()) => Some(Msg::Log(Severity::Info, format!("yanked {} items", len))),
                    Err(err) => Some(Msg::Log(err.severity, err.message)),
                }
            })
        }
        Msg::Paste => {
            let path = model.focused_tree().path()?;
            let provider = model.focused_provider();
            Cmd::single(async move {
                match usecases::copy_from_clipboard(provider, path)
                    .await
                    .map_err(Error::from)
                {
                    Ok(()) => Some(Msg::Batch(vec![
                        Msg::Log(Severity::Debug, format!("pasted items")),
                        Msg::Reload(provider),
                    ])),
                    Err(err) => Some(Msg::Log(err.severity, err.message)),
                }
            })
        }
        Msg::Delete => {
            let tree = model.focused_tree();
            let path = tree.path()?;
            let items = tree
                .current_node
                .selected_items()
                .into_iter()
                .map(|(idx, result)| result.map(|item| (idx, item)))
                .collect::<Result<Vec<_>, _>>()?;
            let provider = model.focused_provider();
            let idx = items.first().map(|(idx, _item)| *idx);
            let paths: Vec<_> = items
                .into_iter()
                .map(|(_, item)| {
                    let mut path = path.clone();
                    path.push(item.name.clone());
                    path
                })
                .collect();
            Cmd::single(async move {
                match usecases::move_to_clipboard(provider, paths)
                    .await
                    .map_err(Error::from)
                {
                    Ok(()) => Some(Msg::Batch(vec![
                        Msg::SetVisual(false),
                        Msg::Reload(provider),
                        Msg::ForceSelect(provider, idx),
                    ])),
                    Err(err) => Some(Msg::Log(err.severity, err.message)),
                }
            })
        }
        Msg::SaveTo(provider, dst) => {
            let items = model
                .focused_tree()
                .current_node
                .selected_items()
                .into_iter()
                .map(|(_, r)| r)
                .collect::<Result<Vec<_>, _>>()?;
            Cmd::single(async move {
                match usecases::save_items(provider, items, dst)
                    .await
                    .map_err(Error::from)
                {
                    Ok(_) => Some(Msg::Reload(provider)),
                    Err(err) => Some(Msg::Log(err.severity, err.message)),
                }
            })
        }
        Msg::CreateDir(name) => {
            let mut path = model.focused_tree().path()?;
            path.push(name);
            let provider = model.focused_provider();
            Cmd::single(async move {
                match usecases::create_dir(provider, path)
                    .await
                    .map_err(Error::from)
                {
                    Ok(_) => Some(Msg::Reload(provider)),
                    Err(err) => Some(Msg::Log(err.severity, err.message)),
                }
            })
        }
        Msg::Rename(new_name) => {
            let item = model
                .focused_tree()
                .current_node
                .selected_single_item()?
                .1?;
            let path = model.focused_tree().path()?;
            let provider = model.focused_provider();
            let src = {
                let mut path = path.clone();
                path.push(item.name.to_owned());
                path
            };
            let dst = {
                let mut path = path.clone();
                path.push(new_name);
                path
            };
            Cmd::single(async move {
                match usecases::rename_item(provider, src, dst)
                    .await
                    .map_err(Error::from)
                {
                    Ok(_) => Some(Msg::Reload(provider)),
                    Err(err) => Some(Msg::Log(err.severity, err.message)),
                }
            })
        }
        Msg::Search(query) => {
            let provider = model.focused_provider();
            let colors = model.colors;
            let tree = model.focused_tree_mut();
            let pages = usecases::search(provider, &query)?;
            *tree = Tree::new(pages).colors(colors);
            tree.current_node
                .load_next_page(
                    |_page| Some(Msg::SelectFirstItem),
                    |err| Some(Msg::Log(err.severity, err.message)),
                )
                .and_then(update(
                    Msg::Log(Severity::Info, format!("searching: {}", &query)),
                    model,
                ))
        }
        Msg::LoadMore => model.focused_tree().current_node.load_next_page(
            move |_page| Some(Msg::Tick),
            move |err| Some(Msg::Log(err.severity, err.message)),
        ),
        Msg::Reload(provider) => {
            let tree = &mut model.trees[provider as usize];
            let selection = tree.current_node.selection().min();
            tree.current_node
                .reload(
                    move |_pages| Some(Msg::ForceSelect(provider, selection)),
                    move |err| Some(Msg::Log(err.severity, err.message)),
                )
                .and_then(update(Msg::Select(provider, None), model))
        }
        Msg::ReloadFocused => {
            let provider = model.focused_provider();
            update(Msg::Reload(provider), model)
        }
        Msg::LoadDetails => model.focused_tree().current_node.load_details(
            |_details| Some(Msg::Tick),
            |err| Some(Msg::Log(err.severity, err.message)),
        )?,
        Msg::RunShCmd(shcmd) => {
            let words = shell_words::split(&shcmd).map_err(|_| Error {
                kind: ErrorKind::Undefined,
                message: format!("invalid shell command"),
                severity: Severity::Warn,
            })?;

            Cmd::single(async move {
                let (cmd, args) = words.split_first().unwrap();
                match Command::new(cmd)
                    .args(args)
                    .stdin(Stdio::piped())
                    .stdout(Stdio::piped())
                    .stderr(Stdio::piped())
                    .spawn()
                    .map_err(Error::from)
                {
                    Ok(child) => Some(Msg::AddProcess(Process::from(child))),
                    Err(err) => Some(Msg::Log(err.severity, err.message)),
                }
            })
            .and_then(update(
                Msg::Log(Severity::Info, format!("running {}", shcmd)),
                model,
            ))
        }
        Msg::AddProcess(mut proc) => {
            let id = proc.id();
            let cmd = proc.wait(
                move |status| {
                    Some(Msg::Batch(vec![
                        Msg::RemoveProcess(id),
                        Msg::Log(
                            Severity::Debug,
                            format!(
                                "process with id `{}` has finished with status {}",
                                id,
                                status.to_string()
                            ),
                        ),
                    ]))
                },
                |err| Some(Msg::Log(err.severity, err.message)),
            );
            model.procs.push(proc);
            cmd
        }
        Msg::RemoveProcess(id) => {
            model.procs.retain(|proc| proc.id() != id);
            Cmd::none()
        }
        Msg::ReplacePlaceholder(expr) => {
            let (prefix, obj, attr, suffix, delim) = {
                let caps = RE_PLACEHOLDER.captures(&expr).unwrap();
                (
                    caps.name("prefix")
                        .map(|m| m.as_str())
                        .unwrap_or_default()
                        .to_owned(),
                    caps.name("obj")
                        .map(|m| m.as_str())
                        .unwrap_or_default()
                        .to_owned(),
                    caps.name("attr")
                        .map(|m| m.as_str())
                        .unwrap_or_default()
                        .to_owned(),
                    caps.name("suffix")
                        .map(|m| m.as_str())
                        .unwrap_or_default()
                        .to_owned(),
                    caps.name("delim")
                        .map(|m| m.as_str())
                        .unwrap_or_default()
                        .to_owned(),
                )
            };

            let elems: BoxStream<String> = match obj.as_str() {
                "item" => {
                    let items = model
                        .focused_tree()
                        .current_node
                        .selected_items()
                        .into_iter()
                        .map(|(_, r)| r)
                        .collect::<Result<Vec<_>, _>>()?;
                    match attr.as_str() {
                        "id" => {
                            stream::iter(items.into_iter().map(|item| item.id.to_owned())).boxed()
                        }
                        "name" => {
                            stream::iter(items.into_iter().map(|item| item.name.to_owned())).boxed()
                        }
                        "kind" => stream::iter(items.into_iter().map(|item| item.kind.to_string()))
                            .boxed(),
                        "path" => {
                            let cwd = model.focused_tree().path()?;
                            stream::iter(
                                items
                                    .into_iter()
                                    .map(|item| -> Result<String, Error> {
                                        let mut path = cwd.clone();
                                        path.push(item.name.to_owned());
                                        Ok(usecases::get_absolute_path(Provider::Storage, path)?
                                            .to_str()
                                            .ok_or(Error {
                                                kind: ErrorKind::Parse,
                                                message: format!("invalid path"),
                                                severity: Severity::Error,
                                            })?
                                            .to_owned())
                                    })
                                    .collect::<Result<Vec<String>, _>>()?,
                            )
                            .boxed()
                        }
                        "links" => {
                            stream::iter(
                                items
                                    .into_iter()
                                    .map(|item| usecases::get_stream_links(item)) // Vec<Result<Stream<Result<String>>>>
                                    .take_while(Result::is_ok)
                                    .map(Result::unwrap), // Vec<Stream<Result<String>>>
                            ) // Stream<Stream<Result<String>>>
                            .flatten() // Stream<Result<String>>
                            .take_while(|result| {
                                let ok = result.is_ok();
                                async move { ok }
                            })
                            .map(|result| result.unwrap()) // Stream<String>
                            .boxed()
                        }
                        attr => Err(Error {
                            kind: ErrorKind::Parse,
                            message: format!("unexpected attr in format field `{}`", attr),
                            severity: Severity::Warn,
                        })?,
                    }
                }
                "log" => match attr.as_str() {
                    "path" => {
                        let path = model
                            .logger
                            .path()
                            .to_str()
                            .ok_or(Error {
                                kind: ErrorKind::Parse,
                                message: format!("invalid path"),
                                severity: Severity::Error,
                            })?
                            .to_owned();
                        stream::once(async move { path }).boxed()
                    }

                    "" => {
                        let logs: Vec<String> = model
                            .logger
                            .get_logs()
                            .into_iter()
                            .map(ToString::to_string)
                            .collect(); // Need collect because to_string needs to take effect. &str doesn't live long enough.
                        stream::iter(logs).boxed()
                    }
                    attr => Err(Error {
                        kind: ErrorKind::Parse,
                        message: format!("unexpected attr in format field `{}`", attr),
                        severity: Severity::Warn,
                    })?,
                },
                "cwd" => {
                    if attr.is_empty() {
                        let path = model
                            .focused_tree()
                            .path()?
                            .to_str()
                            .ok_or(Error {
                                kind: ErrorKind::Parse,
                                message: format!("invalid path"),
                                severity: Severity::Error,
                            })?
                            .to_owned();
                        stream::once(async move { path }).boxed()
                    } else {
                        Err(Error {
                            kind: ErrorKind::Parse,
                            message: format!("unexpected attr in format field `{}`", attr),
                            severity: Severity::Warn,
                        })?
                    }
                }
                obj => {
                    // should be unreachable, but just in case
                    Err(Error {
                        kind: ErrorKind::Parse,
                        message: format!("unexpected object in format field `{}`", obj),
                        severity: Severity::Warn,
                    })?
                }
            };
            let elems = elems
                .then(move |elem| {
                    let prefix = prefix.clone();
                    let suffix = suffix.clone();
                    async move { format!("{}{}{}", prefix, elem, suffix) }
                })
                .boxed();

            Cmd::single(async move {
                let elems: Vec<String> = elems.collect().await;
                match parse_expression(
                    RE_PLACEHOLDER.replace(
                        &expr,
                        &elems
                            .into_iter()
                            .map(|elem| shell_words::quote(&elem).to_string())
                            .collect::<Vec<_>>()
                            .join(&delim),
                    ),
                ) {
                    Ok(msg) => Some(msg),
                    Err(err) => Some(Msg::Log(err.severity, err.message)),
                }
            })
        }
        Msg::Submit => match model.mode {
            // unreachable
            Mode::Normal => Cmd::none(),
            Mode::Interactive(ref mut interactive) => {
                let input = model.text_box.get_text();
                let expr = interactive.submit(input);
                match parse_expression(&expr) {
                    Err(err) => update(Msg::SetMode(Mode::Normal), model)
                        .and_then(update(Msg::Log(err.severity, err.message), model)),
                    Ok(msg) => update(Msg::SetMode(Mode::Normal), model)
                        .and_then(update(Msg::Log(Severity::Info, expr), model))
                        .and_then(update(msg, model)),
                }
            }
        },
        Msg::Batch(msgs) => {
            let mut cmd = Cmd::none();
            for msg in msgs {
                cmd = cmd.and_then(update(msg, model));
            }
            cmd
        }
    })
}

/// Real update that handles errors by logging them. Also logs every message.
fn update(msg: Msg, model: &mut Model) -> Cmd<Msg> {
    model
        .logger
        .log(Severity::Trace, format!("{:?}", &msg))
        .and_then(match try_update(msg, model) {
            Ok(update) => update,
            Err(err) => update(Msg::Log(err.severity.clone(), err.to_string()), model),
        })
}

/// Subscribes to evternal events.
fn subscriptions(model: &Model) -> Sub<Msg> {
    Sub::batch([
        model.terminal.on_event(
            |evt| match evt {
                Event::Key(key) => Msg::KeyPress(key.code),
                Event::Resize(_, _) => Msg::Tick,
                _ => Msg::Log(Severity::Debug, format!("unsupported event")),
            },
            |err| Msg::Log(Severity::Debug, err.to_string()),
        ),
        Sub::batch(
            model
                .trees
                .iter()
                .map(|tree| tree.current_node.on_selected(|_selection| Msg::LoadDetails)),
        ),
        model
            .procs
            .last()
            .map(|proc| {
                Sub::batch([
                    proc.on_stdout(
                        |line| Msg::Log(Severity::Info, line),
                        |err| match err.kind {
                            ErrorKind::IOError(io::ErrorKind::BrokenPipe) => {
                                Msg::Log(Severity::Debug, err.to_string())
                            }
                            _ => Msg::Log(err.severity, err.message),
                        },
                    ),
                    proc.on_stderr(
                        |line| Msg::Log(Severity::Info, line),
                        |err| match err.kind {
                            ErrorKind::IOError(io::ErrorKind::BrokenPipe) => {
                                Msg::Log(Severity::Debug, err.to_string())
                            }
                            _ => Msg::Log(err.severity, err.message),
                        },
                    ),
                ])
            })
            .unwrap_or(Sub::none()),
    ])
}

fn main() {
    let name = env!("CARGO_PKG_NAME");

    // For now this is the only possible location for the config file.
    let config_path = dirs::config_dir()
        .map(|mut path| {
            path.push(name);
            path.push("config.toml");
            path
        })
        .expect("Cannot find configuration directory");

    // Generate default config in case it doesn't exist. Usually after the first launch.
    if !config_path.exists() {
        fs::create_dir_all(config_path.parent().expect("config in root directory"))
            .expect("cannot create config directory");
        let mut config_file = OpenOptions::new()
            .create(true)
            .write(true)
            .open(&config_path)
            .expect("cannot create config file");

        config_file
            .write_all(config::DEFAULT_CONFIG.as_bytes())
            .expect("cannot write config file");
    }

    let config: Config = {
        let mut config_file = BufReader::new(
            OpenOptions::new()
                .read(true)
                .open(&config_path)
                .expect("cannot open config file"),
        );
        let mut buf = String::new();
        config_file
            .read_to_string(&mut buf)
            .expect("cannot read config file");
        toml::from_str(&buf).expect("invalid config file")
    };

    let cache_path = {
        let mut path = CACHE_DIR.to_owned();
        path.push("http-cacache");
        path
    };

    // For now this path is hardcoded.
    let logs_path = {
        let mut path = DATA_DIR.to_owned();
        path.push("log.txt");
        path
    };

    fs::create_dir_all(logs_path.parent().unwrap()).expect("cannot write to given location");
    let logger = Logger::new(logs_path, config.logs.log_level);

    let gogoanime_cfg = gogoanime::ClientConfig {
        url: config.provider.gogoanime.url,
        cache: cache_path,
    };
    gogoanime::init(gogoanime_cfg);

    let clipboard_path = {
        let mut path = CACHE_DIR.to_owned();
        path.push(".clip");
        path
    };

    let storage_cfg = storage::StorageConfig {
        path: config.provider.storage.path,
        clipboard: clipboard_path,
    };
    storage::init(storage_cfg).expect("error initializing local storage");

    let mut trees = Provider::iter().map(|provider| {
        Tree::new(usecases::home(provider).unwrap_or_default()).colors(config.style)
    });

    let trees = [(); Provider::COUNT].map(|_| trees.next().unwrap());

    let is_running = Arc::new(AtomicBool::new(true));

    let model = Model {
        colors: config.style,
        title: name,
        terminal: Terminal::new(),
        keymap: config.keybinds,
        cmd_buff: Vec::new(),
        mode: Mode::Normal,
        text_box: TextBox::default().colors(config.style),
        trees,
        active: [Provider::Storage, Provider::Gogoanime],
        focus: Focus::Left,
        quick_view: true,
        procs: Vec::new(),
        logger,
        is_running: is_running.clone(),
    };

    let init = Cmd::batch([Cmd::batch(Provider::iter().map(|provider| {
        model.trees[provider as usize].current_node.load_next_page(
            move |_page| Some(Msg::Tick),
            move |err| Some(Msg::Log(err.severity, err.message)),
        )
    }))]);

    Program {
        model,
        init,
        update,
        view,
        subscriptions,
    }
    .run_while(is_running);
}
