/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::{
    config::ColorPalette,
    error::{Error, ErrorKind, Severity},
    utils::{tea::Sub, Observable, View},
};
use ratatui::{
    layout::Rect,
    style::Style,
    text::{Line, Span},
    widgets::Paragraph,
    Frame,
};

/// Model struct for the text box.
#[derive(Default)]
pub struct TextBox {
    /// Text that should be displayed.
    text: Observable<String>,

    /// Current cursor position, but not necessarily the index of the string.
    cur_x: u16,
    colors: ColorPalette,
}

#[allow(dead_code)]
impl TextBox {
    /// Returns index of the `cur_x`th character of string
    fn current_index(&self) -> usize {
        let text = self.text.get();
        text.char_indices()
            .map(|(i, _)| i)
            .nth(self.cur_x.into())
            .unwrap_or(text.len())
    }

    pub fn colors(mut self, colors: ColorPalette) -> Self {
        self.colors = colors;
        self
    }

    /// Returns current position of the cursor.
    pub fn cursor_position(&self) -> u16 {
        self.cur_x
    }

    /// Moves cursor to the given position.
    ///
    /// # Arguments
    /// `pos` - Position to which to move the cursor.
    ///
    /// # Errors
    /// When given position is out of range, cursor position will not change and an error will be
    /// returned.
    pub fn move_cursor(&mut self, pos: u16) -> Result<u16, Error> {
        let pos = pos.clamp(0, self.text.get().chars().count() as u16);
        if self.cur_x != pos {
            self.cur_x = pos;
            Ok(pos)
        } else {
            Err(Error {
                kind: ErrorKind::NoChange,
                message: format!("cursor is already at position {}", pos),
                severity: Severity::Debug,
            })
        }
    }

    /// Inserts character at the cursor position and then moves cursor by one step ahead.
    ///
    /// # Arguments
    /// `ch` - Character to be inserted.
    ///
    /// # Errors
    /// If calculated index in not on the char boundary, the state will remain the same and an error
    /// will be returned.
    pub fn insert_char(&mut self, ch: char) -> Result<char, Error> {
        let pos = self.current_index();
        let mut text = self.text.get().to_owned();
        if text.is_char_boundary(pos) {
            text.insert(pos, ch);
            self.cur_x = self.cur_x.saturating_add(1);
            self.text.set(text);
            Ok(ch)
        } else {
            Err(Error {
                kind: ErrorKind::IndexNotOnCharBoundary,
                message: format!("index `{}` is not on a char boundary", pos),
                severity: Severity::Fatal,
            })
        }
    }

    /// Deletes character at the cursor position.
    ///
    /// # Errors
    /// - If calculated index in not on the char boundary, the state will remain the same and an error
    /// will be returned.
    /// - If string is empty
    pub fn forward_delete_char(&mut self) -> Result<char, Error> {
        if self.text.get().is_empty() {
            Err(Error {
                kind: ErrorKind::NoChange,
                message: format!("string is empty"),
                severity: Severity::Debug,
            })
        } else {
            let pos = self.current_index();
            let mut text = self.text.get().to_owned();
            if text.is_char_boundary(pos) {
                let ch = text.remove(pos);
                self.text.set(text);
                Ok(ch)
            } else {
                Err(Error {
                    kind: ErrorKind::IndexNotOnCharBoundary,
                    message: format!("index `{}` is not on a char boundary", pos),
                    severity: Severity::Fatal,
                })
            }
        }
    }

    /// Deletes character before the cursor position and moves cursor one step back.
    ///
    /// # Errors
    /// - If calculated index in not on the char boundary, the state will remain the same and an error
    /// will be returned.
    /// - If string is empty
    pub fn backward_delete_char(&mut self) -> Result<char, Error> {
        if self.text.get().is_empty() {
            Err(Error {
                kind: ErrorKind::NoChange,
                message: format!("string is empty"),
                severity: Severity::Debug,
            })
        } else {
            let backup = self.cur_x;
            self.move_cursor(self.cur_x.saturating_sub(1))?;
            match self.forward_delete_char() {
                Ok(ch) => Ok(ch),
                Err(err) => {
                    // Rollback
                    self.cur_x = backup;
                    Err(err)
                }
            }
        }
    }

    /// Sets the text to the new value.
    pub fn set_text<S>(&mut self, text: S) -> S
    where
        S: ToString,
    {
        let text_str = text.to_string();
        self.cur_x = text_str.chars().count() as u16;
        self.text.set(text_str);
        text
    }

    /// Returns the reference to the current text.
    pub fn get_text(&self) -> &str {
        self.text.get()
    }

    /// Returns a subscription to the text.
    ///
    /// # Arguments
    /// `f` - function that handles the newly set value and returns a message.
    pub fn on_text_changed<M, F>(&self, f: F) -> Sub<M>
    where
        M: Send + 'static,
        F: FnOnce(&String) -> M + 'static,
    {
        Sub::single(async move { self.text.on_next(|text| f(text)).await.unwrap() })
    }
}

/// Controls how text box is displayed.
pub struct TextBoxView {
    pub text: String,
    pub cur_x: u16,
    pub colors: ColorPalette,
}

impl View for TextBoxView {
    fn render_into(self, rect: &mut Frame, area: Rect) {
        rect.set_cursor(area.x + self.cur_x, area.y);

        let view = Paragraph::new(Line::from(vec![Span::styled(self.text, Style::default())]))
            .scroll((0, self.cur_x.saturating_sub(area.width - 1)))
            .style(Style::default());

        rect.render_widget(view, area);
    }
}

/// Returns a view for the given text box
pub fn view(tb: &TextBox) -> TextBoxView {
    TextBoxView {
        text: tb.text.get().to_owned(),
        cur_x: tb.cur_x,
        colors: tb.colors,
    }
}
