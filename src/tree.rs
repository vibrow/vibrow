/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    cmp,
    collections::HashMap,
    mem,
    path::PathBuf,
    sync::{
        atomic::{AtomicBool, AtomicUsize, Ordering},
        Arc, RwLock, RwLockReadGuard,
    },
};

use futures::{stream, StreamExt, TryStreamExt};
use ratatui::{
    layout::Rect,
    style::{Modifier, Style},
    text::{Line, Span, Text},
    widgets::{List, ListItem, ListState},
    Frame,
};
use vibrow_core::{
    model::{Details, Item, Page, Pages},
    usecases,
};

use crate::{
    config::ColorPalette,
    details::DetailsView,
    error::{Error, ErrorKind, Severity},
    utils::{
        tea::{Cmd, Sub},
        Observable, View,
    },
};

/// Represents different kinds of item selection in the tree.
#[derive(Clone, Copy, Debug, Default, PartialEq, PartialOrd)]
pub enum Selection {
    /// Nothing is selected.
    #[default]
    None,

    /// Single item is selected.
    Single(usize),

    /// Items within the range are selected.
    Multiple(usize, usize),
}

#[allow(dead_code)]
impl Selection {
    /// Checks if selection is `None`.
    #[inline]
    pub fn is_none(&self) -> bool {
        matches!(self, Selection::None)
    }

    /// Checks if selection is `Single`
    #[inline]
    pub fn is_single(&self) -> bool {
        matches!(self, Selection::Single(_))
    }

    /// Checks if selection is `Multiple`
    #[inline]
    pub fn is_multiple(&self) -> bool {
        matches!(self, Selection::Multiple(_, _))
    }

    /// Checks if the selection contains certain index.
    #[inline]
    pub fn contains(&self, idx: usize) -> bool {
        match self {
            Selection::None => false,
            Selection::Single(sel) => &idx == sel,
            Selection::Multiple(start, end) => {
                let min = cmp::min(start, end);
                let max = cmp::max(start, end);
                min <= &idx && &idx <= max
            }
        }
    }

    /// Returns the number of selected items.
    #[inline]
    pub fn len(&self) -> usize {
        match self {
            Selection::None => 0,
            Selection::Single(_) => 1,
            Selection::Multiple(start, end) => start.abs_diff(*end) + 1,
        }
    }

    /// Returns the smallest selected index, i.e. the top-most selected item.
    #[inline]
    pub fn min(&self) -> Option<usize> {
        match *self {
            Selection::None => None,
            Selection::Single(idx) => Some(idx),
            Selection::Multiple(start, end) => Some(cmp::min(start, end)),
        }
    }

    /// Returns the largest selected index, i.e. the bottom-most selected item.
    #[inline]
    pub fn max(&self) -> Option<usize> {
        match *self {
            Selection::None => None,
            Selection::Single(idx) => Some(idx),
            Selection::Multiple(start, end) => Some(cmp::max(start, end)),
        }
    }
}

impl IntoIterator for Selection {
    type Item = usize;
    type IntoIter = Box<dyn Iterator<Item = usize>>;
    fn into_iter(self) -> Self::IntoIter {
        match self {
            Selection::None => Box::new(1..=0),
            Selection::Single(idx) => Box::new(idx..=idx),
            Selection::Multiple(start, end) => {
                if start < end {
                    Box::new(start..=end)
                } else {
                    Box::new((end..=start).rev())
                }
            }
        }
    }
}

/// Methods of selecting the item. Since selection is wrapped in Observable, sometimes it is needed
/// to reselect the same item to trigger `on_selected` subscription, and in other cases it's not
/// wanted.
#[derive(Clone, Copy, Debug)]
pub enum Select {
    /// Select no matter whether selection is different or not.
    Always,

    /// Select only if new selection is different from the current.
    IfDifferent,
}

/// A single node of a tree.
pub struct TreeNode {
    /// Pages object which contains instructions on how to load each page.
    pages: Arc<Pages>,

    /// Number of the next page to be loaded.
    next_page: Arc<AtomicUsize>,

    /// Indicates whether a page is being loaded or not.
    is_loading: Arc<AtomicBool>,

    /// Indicates whether there are more pages or not.
    has_more_pages: Arc<AtomicBool>,

    /// Current selection
    selection: Observable<Selection>,

    /// List of loaded items.
    items: Arc<RwLock<Vec<Arc<Item>>>>,

    /// Map of loaded details. Keys are item IDs.
    details: Arc<RwLock<HashMap<String, Arc<Details>>>>,

    /// Scroll position.
    scroll_y: usize,
}

impl TreeNode {
    /// Creates a tree node from `Pages`
    pub fn new(pages: Pages) -> Self {
        Self {
            pages: Arc::new(pages),
            next_page: Arc::new(AtomicUsize::new(1)),
            is_loading: Arc::new(AtomicBool::new(false)),
            has_more_pages: Arc::new(AtomicBool::new(true)),
            selection: Observable::default(),
            items: Arc::new(RwLock::new(Vec::new())),
            details: Arc::new(RwLock::new(HashMap::new())),
            scroll_y: 0,
        }
    }

    /// Returns a list of selected items.
    pub fn items(&self) -> RwLockReadGuard<Vec<Arc<Item>>> {
        self.items.read().unwrap()
    }

    /// Returns details of the selected item.
    ///
    /// # Errors
    /// - if nothing is selected
    /// - if details are not loaded
    /// - see [TreeNode::selected_items] for other cases
    pub fn selected_details(&self) -> Result<Arc<Details>, Error> {
        let item_id = &self
            .selected_items()
            .pop()
            .ok_or(Error {
                kind: ErrorKind::NotSelected,
                message: format!("item is not selected"),
                severity: Severity::Info,
            })?
            .1?
            .id;
        self.details.read()?.get(item_id).cloned().ok_or(Error {
            kind: ErrorKind::NotLoaded,
            message: format!("details are not loaded"),
            severity: Severity::Debug,
        })
    }

    /// Returns `true` if there are more pages to load, `false` otherwise.
    pub fn has_more_pages(&self) -> bool {
        self.has_more_pages.load(Ordering::Relaxed)
    }

    /// Returns `true` if page is currently being loaded, `false` otherwise.
    pub fn is_loading(&self) -> bool {
        self.is_loading.load(Ordering::Relaxed)
    }

    /// Returns current scroll position.
    pub fn scroll_y(&self) -> usize {
        self.scroll_y
    }

    /// Returns current selection.
    pub fn selection(&self) -> Selection {
        self.selection.get().clone()
    }

    /// Returns list of selected items and their indices.
    ///
    /// # Errors
    /// Errors can occur per item, i.e. every item is wrapped in `Result`, but now the whole list.
    /// If for some reason selection is out of range of the list of loaded items, an error is
    /// returned.
    pub fn selected_items(&self) -> Vec<(usize, Result<Arc<Item>, Error>)> {
        self.selection
            .get()
            .into_iter()
            .map(|idx| {
                (
                    idx,
                    self.items().get(idx).cloned().ok_or(Error {
                        kind: ErrorKind::IndexOutOfRange,
                        message: format!("index {} is out of range", idx),
                        severity: Severity::Fatal,
                    }),
                )
            })
            .collect()
    }

    /// Returns selected item only if selection is `Single`.
    ///
    /// # Errors
    /// - Selection is either `None` of `Multiple`
    /// - selection index is out of range
    pub fn selected_single_item(&self) -> Result<(usize, Result<Arc<Item>, Error>), Error> {
        match self.selection() {
            Selection::None => Err(Error {
                kind: ErrorKind::NotSelected,
                message: format!("item not selected"),
                severity: Severity::Info,
            }),
            Selection::Multiple(_, _) => Err(Error {
                kind: ErrorKind::MultipleItemsSelected,
                message: format!("multiple items are selected"),
                severity: Severity::Info,
            }),
            Selection::Single(idx) => Ok((
                idx,
                self.items().get(idx).cloned().ok_or(Error {
                    kind: ErrorKind::IndexOutOfRange,
                    message: format!("index {} is out of range", idx),
                    severity: Severity::Fatal,
                }),
            )),
        }
    }

    /// Crnches `Multiple` selection to a `Single` by leaving only the last selected item selected.
    ///
    /// # Errors
    /// If selection is either `None` or `Single`, nothing is changed and error is returned.
    pub fn switch_to_single_selection(&mut self) -> Result<Selection, Error> {
        if let Selection::Multiple(_, idx) = self.selection() {
            self.selection.set(Selection::Single(idx));
            Ok(self.selection())
        } else {
            Err(Error {
                kind: ErrorKind::NoChange,
                message: format!("selection is already single or none"),
                severity: Severity::Debug,
            })
        }
    }

    /// Changes `Single` selection to `Multiple` by setting first and last selected items to the
    /// currently selected one.
    ///
    /// # Errors
    /// If selection is either `None` or `Multiple`, nothing is changed and error is returned.
    pub fn switch_to_multiple_selection(&mut self) -> Result<Selection, Error> {
        if let Selection::Single(idx) = self.selection() {
            self.selection.set(Selection::Multiple(idx, idx));
            Ok(self.selection())
        } else {
            Err(Error {
                kind: ErrorKind::NoChange,
                message: format!("selection is already multiple or none"),
                severity: Severity::Debug,
            })
        }
    }

    /// Selects given index.
    ///
    /// # Arguments
    /// `idx` - Some index to be set or none.
    /// `method` - Method of selection. See [Select]
    ///
    /// # Errors
    /// If selection doesn't change.
    pub fn select(&mut self, idx: Option<usize>, method: Select) -> Result<Selection, Error> {
        let last = self.items().len().checked_sub(1);
        let curr_selection = self.selection();
        let new_selection = match idx.clamp(None, last) {
            None => Selection::None,
            Some(idx) => match curr_selection {
                Selection::None | Selection::Single(_) => Selection::Single(idx),
                Selection::Multiple(start, _end) => Selection::Multiple(start, idx),
            },
        };

        if matches!(method, Select::IfDifferent) && new_selection == curr_selection {
            Err(Error {
                kind: ErrorKind::NoChange,
                message: format!("already selected"),
                severity: Severity::Debug,
            })
        } else {
            self.selection.set(new_selection);
            self.scroll_y = idx.unwrap_or_default();
            Ok(new_selection)
        }
    }

    /// Returns a subscription for selection.
    ///
    /// # Arguments
    /// `f` - a function that handles new selection and returns a message.
    pub fn on_selected<M, F>(&self, f: F) -> Sub<M>
    where
        M: Send + 'static,
        F: FnOnce(Selection) -> M + 'static,
    {
        Sub::single(async move {
            self.selection
                .on_next(|selection| f(selection.to_owned()))
                .await
                .unwrap()
        })
    }

    /// Returns command that loads details.
    ///
    /// # Arguments
    /// `on_success` - Function that is triggered on successful load. It handles the loaded details
    /// and optionally returns a message.
    ///
    /// `on_error` - Function that is triggered on error. It handles the error and optionally returns
    /// a message.
    ///
    /// # Errors
    /// See [TreeNode::selected_items]
    pub fn load_details<M, OnSuccess, OnError>(
        &self,
        on_success: OnSuccess,
        on_error: OnError,
    ) -> Result<Cmd<M>, Error>
    where
        M: Send + 'static,
        OnSuccess: FnOnce(&Details) -> Option<M> + Send + 'static,
        OnError: FnOnce(Error) -> Option<M> + Send + 'static,
    {
        match self.selected_items().pop() {
            None => Err(Error {
                kind: ErrorKind::NotSelected,
                message: format!("item is not selected"),
                severity: Severity::Debug,
            }),
            Some((_idx, Err(err))) => Err(err),
            Some((_idx, Ok(item))) => match self.details.read()?.get(&item.id) {
                // skip if details are already loaded
                Some(_) => Ok(Cmd::none()),
                None => {
                    let this_details = Arc::clone(&self.details);
                    Ok(Cmd::single(async move {
                        match usecases::get_details(item)
                            .await
                            .map_err(Error::from)
                            .and_then(|details| {
                                this_details
                                    .write()?
                                    .insert(details.item_id.clone(), Arc::new(details.clone()));
                                Ok(details)
                            }) {
                            Ok(details) => on_success(&details),
                            Err(err) => on_error(err.into()),
                        }
                    }))
                }
            },
        }
    }

    /// Returns a command that returns all the data. It will reload as many pages as are currently
    /// loaded.
    ///
    /// # Arguments
    /// `on_success` - Function that is triggered on successful reload. Handles all the loaded pages
    /// and optionally returns a message.
    ///
    /// `on_error` - Function that is triggered on error. Handles the error and optionally returns
    /// a message.
    pub fn reload<M, OnSuccess, OnError>(&self, on_success: OnSuccess, on_error: OnError) -> Cmd<M>
    where
        M: Send + 'static,
        OnSuccess: FnOnce(Vec<Page>) -> Option<M> + Send + 'static,
        OnError: FnOnce(Error) -> Option<M> + Send + 'static,
    {
        let pages = Arc::clone(&self.pages);
        let is_loading = Arc::clone(&self.is_loading);
        let has_more_pages = Arc::clone(&self.has_more_pages);
        let next_page = self.next_page.load(Ordering::Relaxed);
        let items = Arc::clone(&self.items);
        let details = Arc::clone(&self.details);

        async fn reload_(
            pages: Arc<Pages>,
            is_loading: Arc<AtomicBool>,
            has_more_pages: Arc<AtomicBool>,
            next_page: usize,
            items: Arc<RwLock<Vec<Arc<Item>>>>,
            details: Arc<RwLock<HashMap<String, Arc<Details>>>>,
        ) -> Result<Vec<Page>, Error> {
            {
                items.write()?.clear();
                details.write()?.clear();
            }
            is_loading.store(true, Ordering::Relaxed);
            let mut pages = stream::iter(1..next_page)
                .then(|idx| pages.get(idx))
                .try_collect::<Vec<Page>>()
                .await?;
            let mut items = items.write()?;
            for page in &mut pages {
                has_more_pages.store(!page.is_last, Ordering::Relaxed);
                items.extend(&mut page.items.iter().cloned().map(Arc::new));
            }
            is_loading.store(false, Ordering::Relaxed);
            Ok(pages)
        }

        Cmd::single(async move {
            if is_loading.load(Ordering::Relaxed) {
                None
            } else {
                match reload_(pages, is_loading, has_more_pages, next_page, items, details).await {
                    Ok(pages) => on_success(pages),
                    Err(err) => on_error(err),
                }
            }
        })
    }

    /// Returns a command that loads the next page.
    ///
    /// # Arguments
    /// `on_success` - Function that is triggered on successful load. It handles the loaded page and
    /// optionally returns a message.
    ///
    /// `on_error` - Function that is triggered on error. It handles the error and optionally returns
    /// a message.
    pub fn load_next_page<M, OnSuccess, OnError>(
        &self,
        on_success: OnSuccess,
        on_error: OnError,
    ) -> Cmd<M>
    where
        M: Send + 'static,
        OnSuccess: FnOnce(Page) -> Option<M> + Send + 'static,
        OnError: FnOnce(Error) -> Option<M> + Send + 'static,
    {
        let pages = Arc::clone(&self.pages);
        let next_page = Arc::clone(&self.next_page);
        let is_loading = Arc::clone(&self.is_loading);
        let has_more_pages = Arc::clone(&self.has_more_pages);
        let items = Arc::clone(&self.items);
        Cmd::single(async move {
            if is_loading.load(Ordering::Relaxed) {
                None
            } else {
                is_loading.store(true, Ordering::Relaxed);
                let page = pages.get(next_page.load(Ordering::Relaxed)).await;
                if page.is_ok() {
                    next_page.fetch_add(1, Ordering::Relaxed);
                }
                let msg = match page.map_err(Error::from).and_then(|page| {
                    has_more_pages.store(!page.is_last, Ordering::Relaxed);
                    items
                        .write()?
                        .extend(&mut page.items.iter().cloned().map(Arc::new));
                    Ok(page)
                }) {
                    Ok(page) => on_success(page),
                    Err(err) => on_error(err),
                };
                is_loading.store(false, Ordering::Relaxed);
                msg
            }
        })
    }
}

/// A collection of `TreeNode`s. There is one currently active node and a stack of ancestor nodes.
pub struct Tree {
    /// Currently active node.
    pub current_node: TreeNode,

    /// Ancestor nodes.
    pub stack: Vec<TreeNode>,

    pub colors: ColorPalette,
}

impl Tree {
    /// Creates a new tree from `Pages`
    pub fn new(pages: Pages) -> Self {
        Self {
            current_node: TreeNode::new(pages),
            stack: Vec::new(),
            colors: ColorPalette::default(),
        }
    }

    /// Enters the currently selected item, pushing the current node to the stack.
    ///
    /// # Errors
    /// - If exactly one item is not selected.
    /// - If contents of the item cannot be loaded.
    pub fn enter(&mut self) -> Result<&TreeNode, Error> {
        let item = self.current_node.selected_single_item()?.1?;
        let pages = usecases::get_contents(item).map_err(Error::from)?;

        let old_node = mem::replace(&mut self.current_node, TreeNode::new(pages));
        self.stack.push(old_node);
        Ok(&self.current_node)
    }

    /// Leaves the current node, loading the previous node from the stack.
    ///
    /// # Errors
    /// If node doesn't have a parent, nothing is changed and error is returned.
    pub fn leave(&mut self) -> Result<&TreeNode, Error> {
        if let Some(old_node) = self.stack.pop() {
            self.current_node = old_node;
            Ok(&self.current_node)
        } else {
            Err(Error {
                kind: ErrorKind::NoChange,
                message: format!("this node doesn't have a parent"),
                severity: Severity::Debug,
            })
        }
    }

    pub fn colors(mut self, colors: ColorPalette) -> Self {
        self.colors = colors;
        self
    }

    /// Returns all ancestors in form of a path.
    ///
    /// # Errors
    /// If any of the ancestors doesn't have exactly one item selected or the selection is out of
    /// range. The errors shouldn't occur since other functions prevent it.
    pub fn path(&self) -> Result<PathBuf, Error> {
        let mut path = PathBuf::new();
        for elem in self.stack.iter() {
            let name = &elem.selected_single_item()?.1?.name;
            path.push(name);
        }
        Ok(path)
    }
}

/// Controls how tree is displayed.
pub struct ListView<'a> {
    /// List of items
    pub list: List<'a>,

    /// Internal state used only for the scrolling purposes.
    pub state: ListState,
}

impl View for ListView<'_> {
    fn render_into(mut self, rect: &mut Frame, area: Rect) {
        rect.render_stateful_widget(self.list, area, &mut self.state)
    }
}

/// Returns a pair of `ListView` and `DetailsView` for the selected item.
pub fn view(tree: &Tree) -> (ListView, DetailsView) {
    let mut list_items = tree
        .current_node
        .items()
        .iter()
        .enumerate()
        .map(|(idx, item)| {
            let is_selected = tree.current_node.selection().contains(idx);

            let text = Text::from(Line::from(vec![
                Span::styled(format!(" {} ", item.name.to_owned()), Style::default()),
                Span::styled(
                    format!(" {} ", item.kind.to_string()),
                    if is_selected {
                        // Since selected items have colors reversed, setting bg is actually setting fg
                        // fg needs to remain default fg because after reverse the whole line needs to be in the same color
                        Style::default()
                            .bg(tree.colors.accent_1)
                            .add_modifier(Modifier::ITALIC)
                    } else {
                        Style::default()
                            .fg(tree.colors.accent_1)
                            .add_modifier(Modifier::ITALIC)
                            .add_modifier(Modifier::DIM)
                    },
                ),
            ]));

            ListItem::new(text).style(if is_selected {
                Style::default().add_modifier(Modifier::REVERSED)
            } else {
                Style::default()
            })
        })
        .collect::<Vec<_>>();

    if tree.current_node.has_more_pages() && !tree.current_node.is_loading() {
        list_items.push(
            ListItem::new("--- load more ---").style(Style::default().add_modifier(Modifier::DIM)),
        )
    }

    // Add blank rows for scrolling purposes
    while tree.current_node.scroll_y() > list_items.len() {
        list_items.push(ListItem::new(""));
    }

    let lv = ListView {
        list: List::new(list_items),
        state: {
            let mut state = ListState::default();
            state.select(Some(tree.current_node.scroll_y()));
            state
        },
    };

    let dv = DetailsView {
        details: tree
            .current_node
            .selected_details()
            .map(|r| (*r).clone())
            .unwrap_or(Details::default()),
        colors: tree.colors,
    };

    (lv, dv)
}
