/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::sync::{Arc, Mutex};

use futures::{
    channel::mpsc::{self, UnboundedReceiver, UnboundedSender},
    SinkExt, StreamExt,
};

/// An observable data holder. It is designed to be used when you need to represent data that
/// can change over time. `Observable` allows you to observe changes and update the UI accordingly.
pub struct Observable<T> {
    value: Arc<T>,
    tx: UnboundedSender<Arc<T>>,
    rx: Mutex<UnboundedReceiver<Arc<T>>>,
}

#[allow(dead_code)]
impl<T> Observable<T> {
    /// Wraps given value into `Observable`
    ///
    /// # Arguments
    /// `value` - initial value of the observable
    ///
    /// # Examples
    /// ```
    /// let observable = Observable::new(5);
    /// ```
    pub fn new(value: T) -> Self {
        let (mut tx, rx) = mpsc::unbounded();
        let value = Arc::new(value);

        futures::executor::block_on(tx.send(Arc::clone(&value))).ok();
        Self {
            value,
            tx,
            rx: Mutex::new(rx),
        }
    }

    /// Returns a reference to the current value
    ///
    /// # Examples
    /// ```
    /// let observable = Observable::new(5);
    /// assert_eq!(*observable.get(), 5);
    /// ```
    pub fn get(&self) -> &T {
        &self.value
    }

    /// Transforms current value into a new one,
    ///
    /// # Arguments
    /// `f` - A closure that takes a reference to the current value and returns a new one
    ///
    /// # Examples
    /// ```
    /// let mut observable = Observable::new(5);
    /// observable.set_with(|val| val + 1);
    /// assert_eq!(*observable.get(), 6);
    /// ```
    pub fn set_with<F>(&mut self, f: F)
    where
        F: FnOnce(&T) -> T,
    {
        self.set(f(&self.value))
    }

    /// Sets new value and emits it.
    ///
    /// # Arguments
    /// `value` - a new value
    ///
    /// # Examples
    /// ````
    /// let mut observable = Observable::new(5);
    /// observable.set(6);
    /// assert_eq!(*observable.get(), 6);
    /// ````
    pub fn set(&mut self, value: T) {
        let value = Arc::new(value);
        self.value = Arc::clone(&value);
        futures::executor::block_on(self.tx.send(value)).ok();
    }

    /// Waits for the next value and transforms it.
    ///
    /// # Arguments
    /// `f` - A function that takes a reference to the new value and manipulates it
    pub async fn on_next<F, R>(&self, f: F) -> Option<R>
    where
        F: FnOnce(&T) -> R,
    {
        self.rx.lock().unwrap().next().await.map(|t| f(&t))
    }
}

impl<T> Default for Observable<T>
where
    T: Default + Clone,
{
    fn default() -> Self {
        Self::new(T::default())
    }
}

#[cfg(test)]
mod tests {
    use super::Observable;

    #[test]
    fn new_test() {
        Observable::new(5);
    }

    #[test]
    fn get_test() {
        let observable = Observable::new(5);
        assert_eq!(*observable.get(), 5);
    }

    #[test]
    fn set_test() {
        let mut observable = Observable::new(5);
        observable.set(6);
        assert_eq!(*observable.get(), 6);
    }

    #[test]
    fn set_with_test() {
        let mut observable = Observable::new(5);
        observable.set_with(|val| val + 1);
        assert_eq!(*observable.get(), 6);
    }
}
