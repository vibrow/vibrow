/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    pin::Pin,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

use futures::{
    channel::mpsc::{self, UnboundedSender},
    future::{pending, select_all, BoxFuture, Fuse},
    pin_mut, select,
    stream::FuturesUnordered,
    Future, FutureExt, SinkExt, StreamExt,
};

/// An abstract representation of an effectful action. It is used to encapsulate and manage side
/// effects such as accessing filesystem or sending network requests
pub struct Cmd<M>
where
    M: Send,
{
    inner: FuturesUnordered<BoxFuture<'static, Option<M>>>,
}

impl<M> Cmd<M>
where
    M: Send,
{
    /// Creates an empty command
    pub fn none() -> Self {
        Self {
            inner: FuturesUnordered::new(),
        }
    }

    /// Creates a command from a future
    ///
    /// # Arguments
    ///
    /// * `fut` - A future that optionally produces a message
    pub fn single<F>(fut: F) -> Self
    where
        F: Future<Output = Option<M>> + Send + 'static,
    {
        let futs = FuturesUnordered::new();
        futs.push(fut.boxed());
        Self { inner: futs }
    }

    /// Creates a command from a collection of commands. The commands will execute
    /// concurrently and emit messages in no particular order.
    ///
    /// # Arguments
    ///
    /// * `cmds` - A collection of `Cmd`s
    pub fn batch<I>(cmds: I) -> Self
    where
        I: IntoIterator<Item = Cmd<M>>,
    {
        Self {
            inner: cmds.into_iter().map(|cmd| cmd.inner).flatten().collect(),
        }
    }

    /// Creates a batch command from the `self` and the given command
    ///
    /// # Arguments
    ///
    /// * `cmd` - A command to be batched together with `self`
    ///
    /// # Examples
    /// ```
    /// let cmd1 = Cmd::none();
    /// let cmd2 = Cmd::single(async { Some(1) })
    ///
    /// assert_eq!(Cmd::batch([cmd1, cmd2]), cmd1.and_then(cmd2));
    /// ```
    pub fn and_then(self, cmd: Cmd<M>) -> Self {
        Cmd::batch([self, cmd])
    }
}

/// A subscription for events, such as keyboard or mouse events. A subscription allows program to
/// react to these events and trigger updates to the model, which in turn update the view.
///
/// In contrast to Elm, where subscriptions persist throughout the duration of the application, the
/// subscription only lasts for a single iteration and is then rerun. This means that the
/// subscription code is executed once per iteration of the program, rather than being continually
/// active throughout the lifetime of the application. This is because the subscription is
/// implemented as a wrapper for a future that waits for a single event, and then returns a message
/// that triggers update and the next iteration.
pub struct Sub<'a, M> {
    inner: Fuse<Pin<Box<dyn Future<Output = M> + 'a>>>,
}

impl<'a, M> Sub<'a, M> {
    /// An empty subscription that waits forever
    pub fn none() -> Self
    where
        Self: 'a,
    {
        Self::single(pending())
    }

    /// Creates a subscription from a future
    ///
    /// # Arguments
    ///
    /// * `fut` - A future that returns a message
    pub fn single<F>(fut: F) -> Self
    where
        F: Future<Output = M> + 'a,
    {
        Self {
            inner: fut.boxed_local().fuse(),
        }
    }

    /// Creates a subscription from a collection of subscriptions. Only the first message that any
    /// of the subscriptions return is accepted, and others are terminated.
    ///
    /// # Arguments
    ///
    /// * `subs` - A collection of `Sub`s
    pub fn batch<I>(subs: I) -> Self
    where
        I: IntoIterator<Item = Sub<'a, M>> + 'a,
    {
        Self::single(async { select_all(subs.into_iter().map(|sub| sub.inner)).await.0 })
    }
}

/// A core structure of application
pub struct Program<S, M>
where
    M: Send,
{
    /// State of the program
    pub model: S,
    /// A command ran at start
    pub init: Cmd<M>,
    /// Function that updates the state in response to user's actions
    pub update: fn(M, &mut S) -> Cmd<M>,
    /// Function that renders user interface that represents the current state of the application
    pub view: fn(&S),
    /// Function that subscribes program to external events
    pub subscriptions: for<'a> fn(&'a S) -> Sub<'a, M>,
}

/// Runs a command concurrently
///
/// # Arguments
///
/// * `cmd` - A command to execute
/// * `sender` - Sends messages to a channel
fn runcmd<'a, M>(cmd: Cmd<M>, sender: &'a UnboundedSender<M>)
where
    M: Send + 'static,
{
    let sender = sender.to_owned();
    tokio::spawn(
        cmd.inner
            .filter_map(|omsg| async move { omsg })
            .then(move |msg| {
                let mut sender = sender.clone();
                async move { sender.send(msg).await.unwrap_or(()) }
            })
            .collect::<Vec<_>>(),
    );
}

impl<S, M> Program<S, M>
where
    M: Send + 'static,
{
    /// Runs the program, consuming it
    #[tokio::main]
    pub async fn run_while(mut self, running: Arc<AtomicBool>) {
        let (tx, mut rx) = mpsc::unbounded();
        runcmd(self.init, &tx);

        while running.load(Ordering::Relaxed) {
            (self.view)(&self.model);
            let msg: M = {
                let subscriptions = (self.subscriptions)(&self.model).inner;
                let messages = async { rx.next().await.expect("Error receiving") }.fuse();
                pin_mut!(subscriptions, messages);

                select! {
                    m = subscriptions => m,
                    m = messages => m
                }
            };

            let cmd = (self.update)(msg, &mut self.model);
            runcmd(cmd, &tx);
        }
    }
}
