/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    collections::{hash_map::Entry, HashMap},
    hash::Hash,
};

/// Trie is a tree where each each path from the root of the tree to a leaf represents a key. Each
/// node represents a component of the key, and the edges leading out of it represent the next
/// possible key components. Leaves represent values mapped to the keys.
#[derive(Debug, Clone)]
pub enum Trie<K, V> {
    Node(HashMap<K, Trie<K, V>>),
    Leaf(V),
}

#[allow(dead_code)]
impl<'a, K, V> Trie<K, V>
where
    K: Eq + Hash + 'a,
{
    /// Constructs a new, empty TrieMap
    pub fn new() -> Self {
        Trie::Node(HashMap::new())
    }

    /// Constructs a TrieMap from a collection of key-value pairs
    ///
    /// # Arguments
    /// `iter` - A collection of tuples
    pub fn from<I, T>(iter: I) -> Result<Self, OccupiedError>
    where
        I: IntoIterator<Item = (T, V)>,
        T: IntoIterator<Item = K>,
    {
        let mut t = Self::new();
        for (k, v) in iter {
            t.try_insert(k, v)?;
        }
        Ok(t)
    }

    /// Returns a value associated with given key, which is either a Node or a Leaf if it exists or
    /// None if it doesn't.
    ///
    /// # Arguments
    /// `key` - A collection of key components
    pub fn get<I>(&self, key: I) -> Option<&Self>
    where
        I: AsRef<[K]>,
    {
        match self {
            Trie::Node(children) => {
                let key = key.as_ref();
                let (head, tail) = key.split_first().unwrap();
                let node = children.get(&head)?;
                if tail.is_empty() {
                    Some(node)
                } else {
                    node.get(&tail)
                }
            }
            Trie::Leaf(_v) => None,
        }
    }

    /// Inserts a new key-value pair into a TrieMap
    ///
    /// # Arguments
    /// `key` - collection of key components
    /// `value` - value to be mapped to the key
    pub fn try_insert<I>(&mut self, key: I, value: V) -> Result<&mut Self, OccupiedError>
    where
        I: IntoIterator<Item = K>,
    {
        let mut key = key.into_iter();
        let head = key.next().unwrap();
        let tail = key.collect::<Vec<_>>();
        match self {
            Trie::Node(children) => match children.entry(head) {
                Entry::Vacant(entry) => {
                    if tail.is_empty() {
                        Ok(entry.insert(Trie::Leaf(value)))
                    } else {
                        let mut node = Trie::Node(HashMap::new());
                        node.try_insert(tail, value)?;
                        let val = entry.insert(node);
                        Ok(val)
                    }
                }
                Entry::Occupied(entry) => {
                    if tail.is_empty() {
                        Err(OccupiedError)
                    } else {
                        entry.into_mut().try_insert(tail, value)
                    }
                }
            },
            Trie::Leaf(_value) => Err(OccupiedError),
        }
    }
}

#[derive(thiserror::Error, Debug)]
#[error("the key already exists")]
pub struct OccupiedError;

#[cfg(test)]
mod tests {
    use super::{OccupiedError, Trie};

    #[test]
    fn trie_test() -> Result<(), OccupiedError> {
        let mut t = Trie::new();
        t.try_insert("bar".chars(), "value 1")?;
        t.try_insert("baz".chars(), "value 2")?;
        t.try_insert("foo".chars(), "value 3")?;

        assert!(matches!(
            t.get(&"bar".chars().collect::<Vec<char>>()),
            Some(Trie::Leaf("value 1"))
        ));
        assert!(matches!(
            t.get("baz".chars().collect::<Vec<char>>()),
            Some(Trie::Leaf("value 2"))
        ));
        assert!(matches!(
            t.get("foo".chars().collect::<Vec<char>>()),
            Some(Trie::Leaf("value 3"))
        ));
        assert!(matches!(
            t.get("ba".chars().collect::<Vec<char>>()),
            Some(Trie::Node(_c))
        ));
        assert!(matches!(t.get("bza".chars().collect::<Vec<char>>()), None));

        Ok(())
    }
}
