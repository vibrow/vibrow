/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use ratatui::{layout::Rect, Frame};

/// Trait that represents a tui widget. When compared to a built-in `Widget` trait, it is
/// distinguished by its capability to affect how the widget is rendered in a frame. This makes it
/// possible to selectively render different types of widgets depending on runtime conditions
pub trait View {
    fn render_into(self, rect: &mut Frame, area: Rect);
}
